-injars 'SandboxClaim.jar'
-outjars 'SandboxClaimObfuscated.jar'
 
-libraryjars <java.home>/lib/rt.jar
-libraryjars lib/
-libraryjars ../../icerhal-sandboxcore/SandboxCore/SandboxCoreObfuscated.jar
 
-dontshrink
-dontoptimize
-useuniqueclassmembernames
-keepattributes Exceptions,InnerClasses,Signature,Deprecated,SourceFile,LineNumberTable,LocalVariable*Table,*Annotation*,Synthetic,EnclosingMethod
-ignorewarnings
 
 
-keep,allowshrinking class net.IceRhal.SandboxClaim.Main
 
# Keep names - Native method names. Keep all native class/method names.
-keepclasseswithmembers,includedescriptorclasses,allowshrinking class * {
    native <methods>;

    }