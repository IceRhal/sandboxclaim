mkdir -p lib
cd lib
if [ -e spigot.jar ]
then
    echo 'spigot already present'
else
    wget -nc http://spigotmc.fr/download/patched/spigot1658.jar -O spigot.jar
fi
if [ -e permissionsex.jar ]
then
    echo 'permissionsEx already present'
else
    wget -nc http://dev.bukkit.org/media/files/855/302/PermissionsEx-1.23.1.jar -O permissionsex.jar
fi
if [ -e worldguard.jar ]
then
    echo 'worldguard already present'
else
    mkdir tmp
    cd tmp
    wget -nc http://builds.enginehub.org/job/worldguard/6728/download/worldguard-6.0.0-SNAPSHOT.zip -O worldguard.zip
    unzip worldguard.zip
    mv worldguard*.jar ../worldguard.jar
    cd ..
    rm -R tmp;
fi
if [ -e worldedit.jar ]
then
    echo 'worldedit already present'
else
    wget -nc http://builds.enginehub.org/job/worldedit/6710/download/worldedit-bukkit-6.0.2-SNAPSHOT-dist.jar -O worldedit.jar
fi
cd ..
