package net.IceRhal.SandboxClaim;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Paper implements CommandExecutor {
	
	static ItemStack paper;
	static String error = ChatColor.RED + "Erreur dans la commande. " + ChatColor.DARK_RED + "/givepaper <pseudo> <nombre>";

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label,
			String[] args) {
		
		if(label.equalsIgnoreCase("givepaper")) {
			
			if(!net.IceRhal.SandboxCore.Main.a(s, "sandboxclaim.givepaper")) return true;
			
			if(args.length == 0) {
				
				if(s instanceof Player) {
					
					if(((Player) s).getInventory().addItem(paper).isEmpty()) {
						
						s.sendMessage(ChatColor.GREEN + "Tu viens de recevoir un papier de recharge de terrain !");
						return true;
					}
					else {
						
						s.sendMessage(ChatColor.RED + "Ton inventaire est plein.");
						return true;
					}
				}
				else s.sendMessage(error);
			
				return true;
			}
			
			else if(args.length == 1) {
				
				OfflinePlayer p = net.IceRhal.SandboxCore.Main.h(args[0]);
				
				if(p == null) {
					
					try {
						
						Integer nb = Integer.parseInt(args[0]);
						
						if(nb <= 0) {
							
							s.sendMessage(ChatColor.RED + "Le nombre doit être au moins égale à 1.");
							return true;
						}
						
						if(s instanceof Player) {
							
							int x = 0;
							
							while(nb > 0) {
								
								if(((Player) s).getInventory().addItem(paper).isEmpty()) {
									
									x++;
									continue;
								}
								else break;
							}
							
							s.sendMessage(ChatColor.GREEN + "Tu viens de recevoir " + ChatColor.DARK_GREEN + x + ChatColor.GREEN + " papier(s) de recharge de terrains.");
							return true;
						}
						else s.sendMessage(error);
						return true;
						
					} catch(NumberFormatException e) {
						
						s.sendMessage(ChatColor.RED + "Joueur " + ChatColor.DARK_RED + args[0] + ChatColor.RED + " introuvable.");
						return true;
					}
				}
				else if(p.isOnline()) {
					
					Player p2 = Bukkit.getPlayer(p.getUniqueId());
					
					if(p2.getInventory().addItem(paper).isEmpty()) {						
						
						p2.sendMessage(ChatColor.GREEN + "Tu viens de recevoir " + ChatColor.DARK_GREEN + "1" + ChatColor.GREEN
								+ " papier de recharge de terrains.");
					}
					else {
						
						p2.getWorld().dropItem(p2.getLocation(), paper);
						p2.sendMessage(ChatColor.GREEN + "Il y a " + ChatColor.DARK_GREEN + "1" + ChatColor.GREEN
								+ " papier de recharge de terrains qui vient d'apparaitre là où tu es.");
					}
					
					s.sendMessage(ChatColor.GREEN + "Tu viens d'envoyer " + ChatColor.DARK_GREEN + "1" + ChatColor.GREEN 
							+ " papier de recharge de terrains à " + ChatColor.DARK_GREEN);
					return true;
				}
			}
			
			return true;
		}
		else s.sendMessage("Commande Inconnue.");
		return false;
	}
	
	
	

}
