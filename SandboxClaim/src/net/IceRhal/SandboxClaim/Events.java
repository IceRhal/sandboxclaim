package net.IceRhal.SandboxClaim;

import java.util.HashMap;

import net.IceRhal.SandboxCore.f;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.DoubleChest;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.Vector;

public class Events implements Listener {
	
	static HashMap<Player, BlockVector[]> sel = new HashMap<Player, BlockVector[]>();
	FileConfiguration config = Main.config;
	FileConfiguration players = Main.players;
	f cM = Main.cM;
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void InventoryClose(InventoryCloseEvent e) {
		
		Inventory inv = e.getInventory();
		
		if(inv.getName().equals(ChatColor.BOLD + "Interface de Rechargement")) {
			
			Player p = (Player) e.getPlayer();
			
			double i = 0;
			int items = 0;
			boolean mess = false;
			
			if(players.contains("chest")) items = players.getInt("chest");
			
			for(ItemStack is : inv.getContents()) {
				
				if(is != null && is.getType().equals(Material.getMaterial(config.getInt("recharge_item")))) {					
					
					if(players.contains(p.getUniqueId() + "")) i = players.getDouble(p.getUniqueId() + "");
					
					items += is.getAmount();
					i += is.getAmount();
					
					if(i > Main.getExpireBlocs(p.getName()) * 8) {
						
						p.getInventory().addItem(is);
						if(!mess) {		
							
							p.sendMessage(ChatColor.RED + "Tu dépasse ta limite de rechargement de " 
											+ ChatColor.DARK_RED + "2" + ChatColor.RED + " mois. Tu récupère donc ton blé en trop.");
							
							mess = true;
						}
						continue;
					}
					
					players.set(p.getUniqueId() + "", i);
					
					cM.f("players.yml");
				}
				else if(is != null && is.getType().equals(Material.getMaterial(config.getInt("recharge_item2")))) {					
					
					if(players.contains(p.getUniqueId() + "")) i = players.getDouble(p.getUniqueId() + "");
					
					items += is.getAmount() * 9;
					i += is.getAmount() * 9;
					
					if(i > Main.getExpireBlocs(p.getName()) * 8) {
						
						p.getInventory().addItem(is);
						if(!mess) {		
							
							p.sendMessage(ChatColor.RED + "Tu dépasse ta limite de rechargement de " 
											+ ChatColor.DARK_RED + "2" + ChatColor.RED + " mois. Tu récupère donc tes blocs de blé en trop.");
							
							mess = true;
						}
						continue;
					}
					
					players.set(p.getUniqueId() + "", i);
					
					cM.f("players.yml");
				}
				else if(is != null) {
					
					p.getInventory().addItem(is);
					p.sendMessage(ChatColor.RED + "Des objets non désirés pour le rechargement de tes terrains te sont rendu.");
				}
			}
			
			players.set("chest", items);
			cM.f("players.yml");
			
			if(Main.getExpireBlocs(p.getName()) <= 0) {
				
				p.sendMessage(ChatColor.GREEN + "Tu as aucun terrains protégés.");
				return;
			}
			
			int h = (int) (Main.getExpireTime(p.getUniqueId()) / 60);
			int j = h / 24;
			
			if(h < 24) p.sendMessage(ChatColor.GREEN + "Tes terrains expirent dans " + ChatColor.DARK_GREEN + h + ChatColor.GREEN + " heure(s).");
			else {
				
				if(h % 24 == 0) p.sendMessage(ChatColor.GREEN + "Tes terrains expirent dans " + ChatColor.DARK_GREEN + j + ChatColor.GREEN + " jour(s).");
				else p.sendMessage(ChatColor.GREEN + "Tes terrains expirent dans " + ChatColor.DARK_GREEN + j + ChatColor.GREEN + " jour(s) et " + ChatColor.DARK_GREEN + h % 24 + ChatColor.GREEN + " heure(s).");
			}
		}
	}
	
	@EventHandler
	public void ItemSpawn(ItemSpawnEvent e) {
		
		if(e.getEntity().getItemStack().getItemMeta().hasDisplayName() && 
				e.getEntity().getItemStack().getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "Outil de Claim")) 
			e.setCancelled(true);
	}
	
	@EventHandler
	public void PlayerInventoryOpen(InventoryOpenEvent e) {
		
		Player p = (Player) e.getPlayer();
		
		if(Cmd.chest.contains(p)) {
			
			InventoryHolder h = e.getInventory().getHolder();
			Location l;
			
			if(h instanceof Chest) l = ((Chest)h).getLocation();
			else if(h instanceof DoubleChest) l = ((DoubleChest)h).getLocation();
			else return;				
				
			String s = "chest_loc.";
				
			config.set(s + "x", l.getX());
			config.set(s + "y", l.getY());
			config.set(s + "z", l.getZ());
			config.set(s + "world", l.getWorld().getName());
				
			Main.plugin.saveConfig();
				
			Cmd.chest.remove(p);
				
			p.sendMessage(ChatColor.GREEN + "Coffre d'arrivé du blé défini.");
			e.setCancelled(true);
		}
	}
		
	@EventHandler
	public void PlayerInteract(PlayerInteractEvent e) {
		
		Player p = e.getPlayer();
		
		if(p.getItemInHand().getType() == Material.GOLD_AXE) {
			
			if(!Cmd.region.containsKey(p)) {
				
				if(p.getItemInHand().getItemMeta().hasDisplayName() && 
						p.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "Outil de Claim")) 
					p.setItemInHand(new ItemStack(Material.AIR));
				return;
			}
			
			Block b = e.getClickedBlock();
			BlockVector[] bV;
			
			if(e.getAction() == Action.LEFT_CLICK_BLOCK) {
				
				if(Cmd.task.containsKey(p)) return;
				
				if(!sel.containsKey(p)) {
					
					p.sendMessage(ChatColor.AQUA + "Position #1 du terrain définie (x = " + ChatColor.DARK_AQUA + b.getX()
							+ ChatColor.AQUA + ", z = " + ChatColor.DARK_AQUA + b.getZ() + ChatColor.AQUA + ").");
										
					bV = new BlockVector[2];
					
					bV[0] = new BlockVector(b.getX(), 0, b.getZ());				
				}
				else {
					
					bV = sel.get(p);
					
					bV[0] = new BlockVector(b.getX(), 0, b.getZ());
					
					if(bV[1] == null) bV[1] = bV[0];
					
					BlockVector min = Vector.getMinimum(bV[0], bV[1]).toBlockVector();
					BlockVector max = Vector.getMaximum(bV[0], bV[1]).toBlockVector();					
					int peri = (max.getBlockX() - min.getBlockX() + 1) * (max.getBlockZ() - min.getBlockZ() + 1);
					
					if(Cmd.isBig(p, min, max)) {
						
						p.sendMessage(ChatColor.RED + "Séléction trop grande.");
						return;
					}
					
					p.sendMessage(ChatColor.AQUA + "Position #1 du terrain définie (x = " + ChatColor.DARK_AQUA + b.getX()
							+ ChatColor.AQUA + ", z = " + ChatColor.DARK_AQUA + b.getZ() + ChatColor.AQUA + ") ("
							+ ChatColor.DARK_AQUA + peri + ChatColor.AQUA + " bloc(s)).");
					
					Cmd.resetView(p, min, max);
				}
				sel.put(p, bV);
				e.setCancelled(true);
			}
			else if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
										
				if(Cmd.task.containsKey(p)) return;
				
				if(!sel.containsKey(p)) {				

					p.sendMessage(ChatColor.AQUA + "Position #2 du terrain définie (x = " + ChatColor.DARK_AQUA + b.getX()
							+ ChatColor.AQUA + ", z = " + ChatColor.DARK_AQUA + b.getZ() + ChatColor.AQUA + ").");
					
					bV = new BlockVector[2];

					bV[1] = new BlockVector(b.getX(), 0, b.getZ());
				}
				else {
					
					bV = sel.get(p);
					
					bV[1] = new BlockVector(b.getX(), 0, b.getZ());
					
					if(bV[0] == null) bV[0] = bV[1];
					
					BlockVector min = Vector.getMinimum(bV[0], bV[1]).toBlockVector();
					BlockVector max = Vector.getMaximum(bV[0], bV[1]).toBlockVector();					
					int peri = (max.getBlockX() - min.getBlockX() + 1) * (max.getBlockZ() - min.getBlockZ() + 1);
					
					if(Cmd.isBig(p, min, max)) {
						
						p.sendMessage(ChatColor.RED + "Séléction trop grande.");
						return;
					}
					
					p.sendMessage(ChatColor.AQUA + "Position #2 du terrain définie (x = " + ChatColor.DARK_AQUA + b.getX()
							+ ChatColor.AQUA + ", z = " + ChatColor.DARK_AQUA + b.getZ() + ChatColor.AQUA + ") ("
							+ ChatColor.DARK_AQUA + peri + ChatColor.AQUA + " bloc(s)).");
					
					Cmd.resetView(p, min, max);
				}				
				sel.put(p, bV);
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void PlayerJoin(PlayerJoinEvent e) {
		
		final Player p = e.getPlayer();
		double exp = Main.getExpireTime(p.getUniqueId());
		
		if(exp > 0 && Main.getBlocs(p.getName()) > 0) {
			
			int h = (int) (exp / 60);
			final int j = h / 24;
			
			String time1 = ChatColor.DARK_AQUA + "";
			
			if(j < 1) time1 += h + "" + ChatColor.AQUA + " heure(s)";
			else if(h % 24 == 0) time1 += j + "" + ChatColor.AQUA + " jours";
			else time1 += j + "" + ChatColor.AQUA + " jours et " + ChatColor.DARK_AQUA + h % 24 + ChatColor.AQUA + " heure(s)";
			
			final String time = time1;
			
			Bukkit.getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable() {
				@Override
				public void run() {	
					
					if(j <= 7) p.playSound(p.getLocation(), Sound.LEVEL_UP, 100, 0);
					if(j <= 7) p.sendMessage(ChatColor.DARK_RED + "########################################");
					p.sendMessage(ChatColor.AQUA + "Tes claims expirent dans " + time + ".");
					p.sendMessage(ChatColor.GREEN + "Pour recharger tes claims fais " + ChatColor.DARK_GREEN + "/terrain recharger" + ChatColor.GREEN
							+ " et met du blé dans l'inventaire qui va s'ouvrir.");
					if(j <= 7) p.sendMessage(ChatColor.DARK_RED + "########################################");
				}					
			}, 20L);
		}
	}
}
