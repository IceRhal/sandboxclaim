package net.IceRhal.SandboxClaim;

import java.util.HashMap;
import net.IceRhal.SandboxCore.f;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.block.Sign;

import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class Signs implements Listener {
	
	static f cM;
	static FileConfiguration signs;
	
	@EventHandler
	public void SignChange(SignChangeEvent e) {
		
		if(e.getBlock().getType() == Material.WALL_SIGN) {
			
			Sign s = (Sign) e.getBlock().getState();
			
			if(e.getLine(0).equalsIgnoreCase("[Recharge]")) {
								
				Location l = s.getLocation();
				Player p = e.getPlayer();
								
				ProtectedRegion reg = null;
				
				if(!net.IceRhal.SandboxCore.Main.a(p, "sandboxclaim.chest")) return;
				
				LocalPlayer p2 = WGBukkit.getPlugin().wrapPlayer(p);
				
				for(ProtectedRegion r : WGBukkit.getRegionManager(l.getWorld()).getApplicableRegions(l)) {
					
					if(r.getId().startsWith("claim_") && r.getId().length() >= 43 && r.isOwner(p2)) {
						
						reg = r;
						break;
					}
				}
				
				if(reg == null) {
										
					p.sendMessage(ChatColor.RED + "Tu dois être le propriétaire du terrain pour faire ceci.");
					e.setCancelled(true);
				}
				else {
					
					Block b = s.getBlock().getRelative(((org.bukkit.material.Sign)s.getData()).getAttachedFace());
					Block b2 = b.getLocation().add(0, 1, 0).getBlock();
										
					if(b.getType() == Material.HAY_BLOCK && b2.getState() instanceof Chest) {
						
						String path = p.getWorld().getName() + "." + reg.getId().toLowerCase() + ".";
						
						if(signs.contains(path + "x")) {
							
							Block b3 = new Location(p.getWorld(), signs.getInt(path + "x"),
									signs.getInt(path + "y"), signs.getInt(path + "z")).getBlock();
														
							if(b3.getType() == Material.WALL_SIGN) {
								
								Sign s2 = (Sign) b3.getState();
								
								s2.setLine(0, "");
								s2.setLine(1, "");
								s2.setLine(2, "");
								s2.setLine(3, "");
								s2.update();
							}
						}
						
						signs.set(path + "x", l.getBlockX());
						signs.set(path + "y", l.getBlockY());
						signs.set(path + "z", l.getBlockZ());
						cM.f("signs.yml");
						
						int m = (int) Main.getExpireTime(p.getUniqueId());
						int h = m / 60;
						int j = h / 24;
						
						h = (m / 60) - (j * 24);
						
						e.setLine(0, "[Recharge]");
						e.setLine(1, ChatColor.DARK_BLUE + "" + j + ChatColor.DARK_PURPLE + " jours et");
						e.setLine(2, ChatColor.DARK_BLUE + "" + h + ChatColor.DARK_PURPLE + " heures");
						e.setLine(3, ChatColor.DARK_PURPLE + "restant.");
						
						p.sendMessage(ChatColor.GREEN + "Met du blé dans le coffre en haut pour recharger tes terrains !");
					}
					else {
						
						p.sendMessage(ChatColor.RED + "Positionnement du coffre et du bloc de blé incorrect.");
						e.setCancelled(true);
					}
				}								
			}
		}
	}
	
	static HashMap<Sign, String> getSigns() {
		
		HashMap<Sign, String> signMap = new HashMap<Sign, String>();
		
		for(String key : signs.getKeys(true)) {
			
			String[] k = key.split("[.]");
			
			if(k.length == 2) {
								
				World w = Bukkit.getWorld(k[0]);
				
				if(w == null) {
					
					signs.set(k[0], null);
					cM.f("signs.yml");
					continue;
				}
				
				String path = k[0] + "." + k[1] + ".";
				
				RegionManager rM = WGBukkit.getRegionManager(w);				
				
				if(!WGBukkit.getRegionManager(w).hasRegion(k[1]) || !signs.contains(path + "x")) {

					signs.set(k[0] + "." + k[1], null);
					cM.f("signs.yml");
					continue;
				}		
				
				ProtectedRegion r = rM.getRegion(k[1]);

				Location l = new Location(w, signs.getInt(path + "x"), signs.getInt(path + "y"), signs.getInt(path + "z"));
								
				if(!(l.getBlock().getType() == Material.WALL_SIGN &&
						((Sign)l.getBlock().getState()).getLine(0).equalsIgnoreCase("[recharge]"))) {
					
					signs.set(k[0] + "." + k[1], null);
					cM.f("signs.yml");
					continue;
				}

				Sign s = (Sign) l.getBlock().getState();
								
				Block b = s.getBlock().getRelative(((org.bukkit.material.Sign)s.getData()).getAttachedFace());
				Location l2 = b.getLocation();
				l2.add(0, 1, 0);
				
				if(!(r.contains(l.getBlockX(), l.getBlockY(), l.getBlockZ()) && b.getType() == Material.HAY_BLOCK 
						&& l2.getBlock().getState() instanceof Chest)) {
					
					s.setLine(0, "");
					s.setLine(1, "");
					s.setLine(2, "");
					s.setLine(3, "");
					s.update();
					
					signs.set(k[0] + "." + k[1], null);
					cM.f("signs.yml");
					continue;
				}
				
				signMap.put(s, k[1]);
			}
		}
		
		return signMap;
	}
		
	static boolean load() {
		
		cM = new f(Main.plugin);
		
		cM.g("signs.yml");
		
		signs = cM.e("signs.yml");
		
		signs.options().copyDefaults(true);
		cM.f("signs.yml");
		
		return true;
	}

}
