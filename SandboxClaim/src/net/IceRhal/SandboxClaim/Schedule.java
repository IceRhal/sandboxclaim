package net.IceRhal.SandboxClaim;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.history.changeset.ArrayListHistory;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import net.IceRhal.SandboxCore.f;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Schedule extends BukkitRunnable {

    static Schedule inst;

    FileConfiguration players = Main.players;
    f cM = Main.cM;

    public Schedule() {

        inst = this;
    }

    @Override
    public void run() {

        UUID id = CheckClaim.queue.peek();

        if(id == null) Bukkit.getScheduler().cancelTask(getTaskId());

        for(RegionManager rM : WGBukkit.getPlugin().getRegionContainer().getLoaded()) {

            for(ProtectedRegion r : rM.getRegions().values()) {

                if(r.isOwner(WGBukkit.getPlugin().wrapOfflinePlayer(Bukkit.getOfflinePlayer(id)))) {

                    List<String> expireList;

                    if(players.contains("expireList")) expireList = players.getStringList("expireList");
                    else expireList = new ArrayList<String>();

                    BlockVector min = r.getMinimumPoint();
                    BlockVector max = r.getMaximumPoint();

                    expireList.add(id + "*" + r.getId().toLowerCase().substring(43) + "*" + (max.getBlockX() + min.getBlockX()) / 2 + "*"
                            + (max.getBlockZ() + min.getBlockZ()) / 2 + "*" + rM.getName());

                    players.set("expireList", expireList);
                    cM.f("players.yml");

                    rM.removeRegion(r.getId());
                }
            }
        }
    }

    static boolean haveInstance() {

        if(inst == null || !Bukkit.getScheduler().isCurrentlyRunning(inst.getTaskId())) return false;
        else return true;
    }
}
