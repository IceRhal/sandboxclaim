package net.IceRhal.SandboxClaim;

import java.util.ArrayList;
import java.util.UUID;

import net.IceRhal.SandboxCore.f;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;


import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.flags.DoubleFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class Main extends JavaPlugin {

	static Main plugin;
	static f cM;
	static FileConfiguration players;
	static FileConfiguration config;
	static StateFlag claimable = new StateFlag("claimable", false);
	static DoubleFlag multiplier = new DoubleFlag("multiplier");
	
	@SuppressWarnings("deprecation")
	public void onEnable() {	
			
		plugin = this;
		cM = new f(plugin);
		config = getConfig();
		
		cM.g("players.yml");
		
		players = cM.e("players.yml");
		
		config.options().copyDefaults(true);
		cM.f("players.yml");
		saveConfig();
						
		getCommand("claim").setExecutor(new Cmd());
		getCommand("terrain").setExecutor(new Cmd());
		getCommand("adminclaim").setExecutor(new Cmd());
		getCommand("aclaim").setExecutor(new Cmd());	
		getCommand("givepaper").setExecutor(new Paper());
		
		getCommand("claim").setTabCompleter(new TabCmd());
		getCommand("terrain").setTabCompleter(new TabCmd());
		getCommand("adminclaim").setTabCompleter(new TabCmd());
		getCommand("aclaim").setTabCompleter(new TabCmd());
		
		getServer().getPluginManager().registerEvents(new Events(), plugin);
		getServer().getPluginManager().registerEvents(new Signs(), plugin);

	    if(!(Bukkit.getPluginManager().getPlugin("WorldGuard") instanceof WorldGuardPlugin)) {
	    	
	    	System.out.print("WorldGuard introuvable.");
	    	getPluginLoader().disablePlugin(plugin);
	    }
	    
	    Bukkit.getScheduler().runTaskTimer(this, new CheckClaim(), 0L, 60 * 60 * 20L);
	    
	    ItemStack is = new ItemStack(Material.GOLD_AXE, 1);
	   
	    ItemMeta iM = is.getItemMeta();
	   
	    iM.setDisplayName(ChatColor.YELLOW + "Outil de Claim");
	    is.setItemMeta(iM);
	   
	    Cmd.axe = is;
	    
	    is = new ItemStack(Material.PAPER);
	    
	    iM = is.getItemMeta();
	    
	    iM.setDisplayName(ChatColor.AQUA + "Papier de Recharge 1 semaine");
	    
	    ArrayList<String> ls = new ArrayList<String>();
	    
	    ls.add(ChatColor.GOLD + "Ce papier te permet de recharger une semaine de protection de terrain.");
	    ls.add(ChatColor.GOLD + "Pour l'utiliser met le dans un inventaire de recharge (/terrain recharger)");
	    
	    iM.setLore(ls);	    
	    is.setItemMeta(iM);
	    
	    Paper.paper = is;
	    
	    net.IceRhal.SandboxCore.Main.a(claimable);
	    net.IceRhal.SandboxCore.Main.a(multiplier);
	    
	    Signs.load();
	}
	
	public void onDisable() {
		
		
	}
	
	static public int getBlocs(String pName) {
		
		int blocs = 0;
		
		LocalPlayer p = WGBukkit.getPlugin().wrapOfflinePlayer(net.IceRhal.SandboxCore.Main.h(pName));
		
		for(RegionManager rM : WGBukkit.getPlugin().getRegionContainer().getLoaded()) {
			
			for(ProtectedRegion r : rM.getRegions().values()) {
				
				if(r.isOwner(p)) {
					
					BlockVector max = r.getMaximumPoint();
					BlockVector min = r.getMinimumPoint();
					
					blocs += (max.getBlockX() - min.getBlockX() + 1) * (max.getBlockZ() - min.getBlockZ() + 1);					
				}
			}
		}
		
		return blocs;
	}
	
	static public double getExpireBlocs(String pName) {
		
		double blocs = 0;
		
		LocalPlayer p = WGBukkit.getPlugin().wrapOfflinePlayer(net.IceRhal.SandboxCore.Main.h(pName));
		
		for(RegionManager rM : WGBukkit.getPlugin().getRegionContainer().getLoaded()) {
						
			for(ProtectedRegion r : rM.getRegions().values()) {

				try { 
				
					if(r != null && r.getOwners().size() > 0 && r.isOwner(p)) {

						BlockVector max = r.getMaximumPoint();
						BlockVector min = r.getMinimumPoint();

						double multi = 1;
						ProtectedRegion r2 = null;
					
						for(ProtectedRegion r3 : rM.getApplicableRegions(r)) {
						
							if((r2 == null || r2.compareTo(r3) == 1) && r3.getFlags().containsKey(multiplier)) {
							
								multi = r3.getFlag(multiplier);
							}						
						}
					
						blocs += (max.getBlockX() - min.getBlockX() + 1) * (max.getBlockZ() - min.getBlockZ() + 1) * multi;					
					}
				} catch(NullPointerException e) {
					
					return 0;
				}
			}
		}
		
		return blocs;
	}
	
	static public double getExpireTime(UUID id) {
		
		OfflinePlayer p = Bukkit.getOfflinePlayer(id);
		if(p == null) return 0;
		double i = 0;
		
		if(players.contains(id + "")) i = players.getDouble(id + "");
		double m = ((168 * i) / getExpireBlocs(p.getName()) * 60);
								
		return m;
	}
	
	@SuppressWarnings("deprecation")
	static int getLimit(String playerName, World w) {
		
		int limit = -1;
		PermissionUser p = PermissionsEx.getUser(playerName);
		
		for(String grp : p.getGroupNames()) {

			for(String perm : PermissionsEx.getPermissionManager().getGroup(grp).getPermissions(w.getName())) {
				
				String[] per = perm.split("[.]");
				
				if(per.length == 3 && per[0].equalsIgnoreCase("claim") && per[1].equalsIgnoreCase("limit")) {
	
					try {
												
						if(p.has("claim.limit." + per[2])) limit = Integer.parseInt(per[2]);
						
					} catch(NumberFormatException e) {}					
				}
			}
		}
		
		if(p.has("claim.limit.infinite")) limit = -1;
		
		return limit;
	}
}
