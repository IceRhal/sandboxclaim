package net.IceRhal.SandboxClaim;

import java.util.*;
import java.util.concurrent.DelayQueue;

import net.IceRhal.SandboxCore.f;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.block.DoubleChest;
import org.bukkit.block.Sign;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class CheckClaim extends BukkitRunnable {

	FileConfiguration config = Main.config;
	FileConfiguration players = Main.players;
	f cM = Main.cM;

    static Queue<UUID> queue = new LinkedList<UUID>();

	@SuppressWarnings("deprecation")
	@Override
	public void run() {
		
		System.out.print("[DEBUG] Check Claim Start");
		
		double w = -1;
		
		if(players.contains("last_check")) w = (double) (new Date().getTime() - players.getLong("last_check")) / 1000 / 60 / 60 / 24 / 7;
		else {
			
			players.set("last_check", new Date().getTime());
			cM.f("players.yml");
		}	
		
		Date d = new Date();
		
		d.setHours(6);
		
		//System.out.print(w + " " + d.getTime());
		
		if(w > 0) {
			
			players.set("last_check", new Date().getTime());
			cM.f("players.yml");
		}
		else w = -1;
		
		ArrayList<UUID> uuid = new ArrayList<UUID>();
		
		for(RegionManager rM : WGBukkit.getPlugin().getRegionContainer().getLoaded()) {
			
			for(ProtectedRegion r : rM.getRegions().values()) {

				if(r.getId().startsWith("claim_")) {
					
					UUID id = UUID.fromString(r.getId().substring(6, 42));
					
					if(!uuid.contains(id)) uuid.add(id);
				}
			}
		}		
		
		for(UUID id : uuid) {

			double expire = Main.getExpireTime(id);
			
			if(expire <= 0) {
							
				queue.offer(id);

                if(!Schedule.haveInstance()) new Schedule().runTaskTimer(Main.plugin, 0L, 5L);
			}
			else if(w > 0) {
				
				double rest = players.getDouble(id + "") - (double) Main.getBlocs(Bukkit.getOfflinePlayer(id).getName()) * w;
								
				players.set(id + "", rest);
				cM.f("players.yml");
			}
		}
		
		for(Sign s : Signs.getSigns().keySet()) {
			
			Block b = s.getBlock().getRelative(((org.bukkit.material.Sign)s.getData()).getAttachedFace())
						.getLocation().add(0, 1, 0).getBlock();
			
			if(b.getState() instanceof Chest) {
								
				Inventory inv = ((Chest) b.getState()).getInventory();
				
				double i = 0;
				int items = 0;
				OfflinePlayer p = Bukkit.getOfflinePlayer(UUID.fromString(Signs.getSigns().get(s).substring(6, 42)));
				
				if(players.contains("chest")) items = players.getInt("chest");
				
				for(ItemStack is : inv.getContents()) {
										
					if(is != null && is.getType().equals(Material.getMaterial(config.getInt("recharge_item")))) {					
						
						if(players.contains(p.getUniqueId() + "")) i = players.getDouble(p.getUniqueId() + "");
						
						i += is.getAmount();
						items += is.getAmount();
						
						if(i > Main.getExpireBlocs(p.getName()) * 8) continue;
						
						inv.remove(is);
						players.set(p.getUniqueId() + "", i);
						
						cM.f("players.yml");
					}
					else if(is != null && is.getType().equals(Material.getMaterial(config.getInt("recharge_item2")))) {					
						
						if(players.contains(p.getUniqueId() + "")) i = players.getDouble(p.getUniqueId() + "");
						
						i += is.getAmount() * 9;
						items += is.getAmount() * 9;
						
						if(i > Main.getExpireBlocs(p.getName()) * 8) continue;
						
						inv.remove(is);
						players.set(p.getUniqueId() + "", i);
						
						cM.f("players.yml");
					}
				}
				
				players.set("chest", items);
				cM.f("players.yml");
			}	
			
			int m = (int) Main.getExpireTime(UUID.fromString(Signs.getSigns().get(s).substring(6, 42)));
			int h = m / 60;
			int j = h / 24;
			
			h = (m / 60) - (j * 24);
			
			s.setLine(0, "[Recharge]");
			s.setLine(1, ChatColor.DARK_BLUE + "" + j + ChatColor.DARK_PURPLE + " jours et");
			s.setLine(2, ChatColor.DARK_BLUE + "" + h + ChatColor.DARK_PURPLE + " heures");
			s.setLine(3, ChatColor.DARK_PURPLE + "restant.");
			s.update();
		}		
		
		if(config.contains("chest_loc.world")) {
			
			String s = "chest_loc.";
						
			if(Bukkit.getWorld(config.getString(s + "world")) == null) return;
			
			BlockState b = Bukkit.getWorld(config.getString(s + "world")).getBlockAt(config.getInt(s + "x"), 
					config.getInt(s + "y"), config.getInt(s + "z")).getState();
			
			if(b != null) {
				
				final Inventory inv;
				
				if(b instanceof Chest) inv = ((Chest) b).getBlockInventory();
				else if(b instanceof DoubleChest) inv = ((DoubleChest) b).getInventory();
				else return;
				
				if(putChest(inv)) {
					
					Bukkit.getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable() {
						
						@Override
						public void run() {
							
							if(putChest(inv)) {
								
								Bukkit.getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable() {
									
									@Override
									public void run() {

										putChest(inv);										
									}									
								}, 15 * 60 * 20L);					
							}									
						}						
					}, 15 * 60 * 20L);					
				}				
			}
		}
		
		System.out.print("[DEBUG] Check Claim Stop");		
	}
	
	/*public void Schedule(final UUID id, final int min) {
		
		System.out.print("[DEBUG] " + "Schedule " + Bukkit.getOfflinePlayer(id).getName());
				
		Bukkit.getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable() {

			@Override
			public void run() {
				
				double expire = Main.getExpireTime(id);
				boolean boucle = true;
				
				if(expire > min) boucle = false;				
				
				if(expire <= 0) {
					
					for(RegionManager rM : WGBukkit.getPlugin().getRegionContainer().getLoaded()) {
						
						for(ProtectedRegion r : rM.getRegions().values()) {
							
							if(r.isOwner(WGBukkit.getPlugin().wrapOfflinePlayer(Bukkit.getOfflinePlayer(id)))) {
								
								List<String> expireList;
								
								if(players.contains("expireList")) expireList = players.getStringList("expireList");
								else expireList = new ArrayList<String>();
								
								BlockVector min = r.getMinimumPoint();
								BlockVector max = r.getMaximumPoint();
								
								expireList.add(id + "*" + r.getId().toLowerCase().substring(43) + "*" + (max.getBlockX() + min.getBlockX()) / 2 + "*" 
										+ (max.getBlockZ() + min.getBlockZ()) / 2 + "*" + rM.getName());
								
								players.set("expireList", expireList);
								cM.f("players.yml");
								
								rM.removeRegion(r.getId());
							}
						}
					}
					
					boucle = false;
				}
				
				OfflinePlayer p1 = Bukkit.getOfflinePlayer(id);
				
				if(p1.isOnline()) {
					
					Player p = Bukkit.getPlayer(p1.getUniqueId());
					
					if(expire == 30 || expire == 15 || (expire <= 10 && expire > 0)) {
						
						p.sendMessage(ChatColor.RED + "Tes claims expirent dans " + ChatColor.DARK_RED + (int) expire + ChatColor.RED + " minute(s).");
						p.playSound(p.getPlayer().getLocation(), Sound.ORB_PICKUP, 100, 1);
					}
					else if(expire == 0) {
						
						p.getPlayer().sendMessage(ChatColor.RED + "Tes claims viennent d'expirer.");
					}
				}
				
				double rest = players.getDouble(id + "") - (double) Main.getBlocs(p1.getName()) / 168 / 60;
						
				players.set(id + "", rest);
				cM.f("players.yml");

				if(!boucle) return;
				else Schedule(id, min - 1);
			}			
		}, 60 * 20L);
	}*/
	
	boolean putChest(Inventory inv) {
		
		int rest = 0;
		
		for(int i = players.getInt("chest"); i > 0; i--) {
			
			if(inv.firstEmpty() == -1) {
				
				rest = i;
				break;
			}
			inv.addItem(new ItemStack(Material.WHEAT, 1));
		}
		
		players.set("chest", rest);
		cM.f("players.yml");
		
		if(rest > 0) return true;
		else return false;
	}
}
