package net.IceRhal.SandboxClaim;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class TabCmd implements TabCompleter {

	@SuppressWarnings("deprecation")
	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		
		ArrayList<String> s = new ArrayList<String>();
		ArrayList<String> s2 = new ArrayList<String>();		

		if(!(sender instanceof Player)) return s;	
		
		Player p = (Player) sender;
		
		if(!(p.hasPermission("claim.admin") || !p.hasPermission("claim.user"))) return s;
				
		if(label.equalsIgnoreCase("claim") || label.equalsIgnoreCase("terrain")) {
			
			if(args.length == 1) {
				
				s2.add("confirmer");
				s2.add("vider");
				s2.add("ajouter");
				s2.add("enlever");
				s2.add("liste");
				s2.add("info");
				s2.add("recharger");
				s2.add("agrandir");
				s2.add("reduire");
				s2.add("agrandirtous");
				s2.add("reduiretous");
				s2.add("selectionner");
				s2.add("supprimer");
				s2.add("aide");
				
				if(!args[0].equalsIgnoreCase("") && label.equalsIgnoreCase("claim")) {
					
					s2.add("confirm");
					s2.add("clear");
					s2.add("add");
					s2.add("remove");
					s2.add("list");
					s2.add("recharge");
					s2.add("expand");
					s2.add("contract");
					s2.add("expandall");
					s2.add("contractall");
					s2.add("select");
					s2.add("delete");
					s2.add("help");
				}
			
				for(String str : s2) {
					
					if(str.startsWith(args[0].toLowerCase())) s.add(str);
				}
			}
			else if(args.length >= 2) {
											
				if(args[0].equalsIgnoreCase("info") || args[0].equalsIgnoreCase("selectionner") 
						|| args[0].equalsIgnoreCase("select") || args[0].equalsIgnoreCase("supprimer")
						|| args[0].equalsIgnoreCase("delete")) {					
					
					if(args.length == 2) {

						for(ProtectedRegion r : getClaim(p.getName())) {
							
							if(r.getId().substring(43).startsWith(args[1])) {
								
								s.add(r.getId().substring(43));
							}
						}
					}
					
					if(args[0].equalsIgnoreCase("info") && args.length == 3) {
						
						for(ProtectedRegion r : getClaim(p.getName())) {
							
							for(String str : r.getMembers().getPlayers()) {
								
								if(!s2.contains(str)) s2.add(str);
							}
						}
						
						int c = 4 + s2.size();
						
						if(Main.getBlocs(p.getName()) > 0) c++;
						if(Main.getLimit(p.getName(), p.getWorld()) >= 0) c++;
						
						int pmax = c / 9;
						if(c % 9 != 0) pmax++;
						
						int nb;
						
						try {
							
							nb = Integer.parseInt(args[1]);
							
							if(nb < 0 && nb > pmax) nb = 1;
						} catch(NumberFormatException e) {
							
							nb = 1;
						}
						
						for(int x = 0; x < 10; x++) {
							
							if((nb + x) < pmax) {
								
								s.add(nb + x + "");
							}
						}
						
						s.add(pmax + "");
					}
				}
				else if(args[0].equalsIgnoreCase("ajouter") || args[0].equalsIgnoreCase("add")
						|| args[0].equalsIgnoreCase("enlever") || args[0].equalsIgnoreCase("remove")) {

					if(args.length == 2) {
						
						for(OfflinePlayer p2 : Bukkit.getOfflinePlayers()) {
							
							if(p2.getName().toLowerCase().startsWith(args[1].toLowerCase())) {
								
								for(ProtectedRegion r : getClaim(p.getName())) {
						
									if(s.contains(p2.getName())) break;
																		
									if((args[0].equalsIgnoreCase("ajouter") || args[0].equalsIgnoreCase("add")) 
											&& !r.isMember(p2.getName())) s.add(p2.getName());
									else if((args[0].equalsIgnoreCase("enlever") || args[0].equalsIgnoreCase("remove")) 
											&& r.isMember(p2.getName()) && !p.getName().equalsIgnoreCase(p2.getName())) 
											s.add(p2.getName());
								}
							}
							
							if(s.size() >= 10) break;
						}
					}
					else if(args.length == 3) {
						
						if("*".startsWith(args[2])) s.add("*");					
						
						for(ProtectedRegion r : getClaim(p.getName())) {
								
							if(!s.contains(r.getId().substring(43).startsWith(args[2])) && ((args[0].equalsIgnoreCase("ajouter") 
									|| args[0].equalsIgnoreCase("add")) && !r.getMembers().contains(args[1])) 
									&& ((args[0].equalsIgnoreCase("enlever") || args[0].equalsIgnoreCase("remove") 
											&& r.getMembers().contains(args[1])))) {
								
								s.add(r.getId().substring(43));
							}
						}
					}
				}
				else if(args[0].equalsIgnoreCase("liste") || args[0].equalsIgnoreCase("list")) {
					
					if(args.length == 2) {
						
						int c = getClaim(p.getName()).size() + 2;
						
						if(Main.getBlocs(p.getName()) > 0) c++;
						if(Main.getLimit(p.getName(), p.getWorld()) >= 0) c++;
						
						int pmax = c / 9;
						if(c % 9 != 0) pmax++;
						
						int nb;
						
						try {
							
							nb = Integer.parseInt(args[1]);
							
							if(nb < 0 && nb > pmax) nb = 1;
						} catch(NumberFormatException e) {
							
							nb = 1;
						}
						
						for(int x = 0; x < 10; x++) {
							
							if((nb + x) < pmax) {
								
								s.add(nb + x + "");
							}
						}
						
						s.add(pmax + "");
					}					
				}			
			}
		}
		else if(label.equalsIgnoreCase("aclaim") || label.equalsIgnoreCase("adminclaim") && p.hasPermission("claim.admin")) {
			
			if(args.length == 1) {
				
				s2.add("help");
				s2.add("list");
				s2.add("info");
				s2.add("expire");
				if(p.hasPermission("claim.delete")) s2.add("delete");
				if(p.hasPermission("claim.addcredit")) s2.add("addcredit");
				if(p.hasPermission("claim.setchest")) s2.add("setchest");				
			
				for(String str : s2) {
					
					if(str.startsWith(args[0])) s.add(str);
				}
			}
			else if(args.length >= 2) {
				
				if(args[0].equalsIgnoreCase("delete") || args[0].equalsIgnoreCase("list")
						|| args[0].equalsIgnoreCase("info") || args[0].equalsIgnoreCase("addcredit")) {
					
					if(args.length == 2) {
						 
						for(RegionManager rM : WGBukkit.getPlugin().getRegionContainer().getLoaded()) {
							
							for(ProtectedRegion reg : rM.getRegions().values()) {

								if(s.size() >= 10) break;
								
								for(String str : reg.getOwners().getPlayers()) {
									
									if(s.size() >= 10) break;
									
									OfflinePlayer p2 = Bukkit.getOfflinePlayer(str);
									
									if(p.hasPlayedBefore() && !s.contains(p2.getName()) 
											&& p2.getName().toLowerCase().startsWith(args[1].toLowerCase())) {
										
										s.add(p2.getName());
									}
								}								

								if(s.size() >= 10) break;
								
								for(String str : reg.getMembers().getPlayers()) {
									
									if(s.size() >= 10) break;
									
									OfflinePlayer p2 = Bukkit.getOfflinePlayer(str);
									
									if(p2.hasPlayedBefore() && !s.contains(p2.getName())
										&& p2.getName().toLowerCase().startsWith(args[1].toLowerCase())) {
										
										s.add(p2.getName());
									}
								}					

								if(s.size() >= 10) break;
							}
						}
						
						if(s2.size() < 10 && args[0].equalsIgnoreCase("addcredit")) {
							
							for(OfflinePlayer p2 : Bukkit.getOfflinePlayers()) {
								
								if(p2.getName().toLowerCase().startsWith(args[1].toLowerCase()) && !s.contains(p2.getName())) {
									
									s.add(p2.getName());  
								}
								
								if(s.size() >= 10) break;
							}
						}
					}
					else if(args.length == 3) {
																		
						String p2 = Bukkit.getOfflinePlayer(args[1]).getName();
						
						if(args[0].equalsIgnoreCase("delete") || args[0].equalsIgnoreCase("info")) {
							
							if("*".startsWith(args[2].toLowerCase()) && args[0].equalsIgnoreCase("delete")) s.add("*");
							
							for(ProtectedRegion r : getClaim(p2)) {
								
								if(!s.contains(r.getId().substring(43).toLowerCase().startsWith(args[2].toLowerCase()))) 
									s.add(r.getId().substring(43));
							}
						}
						else if(args[0].equalsIgnoreCase("list")) {
														
							int c = getClaim(p2).size() + 2;
							
							if(Main.getBlocs(p2) > 0) c++;
							if(Main.getLimit(p2, p.getWorld()) >= 0) c++;
							
							int pmax = c / 9;
							if(c % 9 != 0) pmax++;
							
							int nb;
							
							try {
								
								nb = Integer.parseInt(args[2]);
								
								if(nb < 0 && nb > pmax) nb = 1;
							} catch(NumberFormatException e) {
								
								nb = 1;
							}
							
							for(int x = 0; x < 10; x++) {
								
								if((nb + x) < pmax) {
									
									s.add(nb + x + "");
								}
							}
							
							s.add(pmax + "");
						}
					}
					else if(args.length == 4 && args[0].equalsIgnoreCase("info")) {

						String p2 = Bukkit.getOfflinePlayer(args[1]).getName();
						
						for(ProtectedRegion r : getClaim(p2)) {
							
							for(String str : r.getMembers().getPlayers()) {
								
								if(!s2.contains(str)) s2.add(str);
							}
						}
						
						int c = 4 + s2.size();
						
						if(Main.getBlocs(p2) > 0) c++;
						if(Main.getLimit(p2, p.getWorld()) >= 0) c++;
						
						int pmax = c / 9;
						if(c % 9 != 0) pmax++;
						
						int nb;
						
						try {
							
							nb = Integer.parseInt(args[3]);
							
							if(nb < 0 && nb > pmax) nb = 1;
						} catch(NumberFormatException e) {
							
							nb = 1;
						}
						
						for(int x = 0; x < 10; x++) {
							
							if((nb + x) < pmax) {
								
								s.add(nb + x + "");
							}
						}
						
						s.add(pmax + "");
					}
				}
				else if(args[0].equalsIgnoreCase("expire")) {
					
					if(args.length == 2) {
						
						s2.add("remove");
						if(p.hasPermission("claim.expire.tp")) s2.add("tp");
						
						for(String str : s2) {
							
							if(str.startsWith(args[1])) s.add(str);
						}
					}
				}				
			}
		}
		
		return s;
	}
	
	@SuppressWarnings("deprecation")
	ArrayList<ProtectedRegion> getClaim(String pName) {
		
		ArrayList<ProtectedRegion> r = new ArrayList<ProtectedRegion>();
		
		for(RegionManager rM : WGBukkit.getPlugin().getRegionContainer().getLoaded()) {
			
			for(ProtectedRegion reg : rM.getRegions().values()) {
				
				if(reg.isOwner(pName)) {
					
					r.add(reg);
				}
			}
		}
		
		return r;
	}

}
