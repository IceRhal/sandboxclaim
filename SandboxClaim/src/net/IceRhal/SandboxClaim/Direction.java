package net.IceRhal.SandboxClaim;

import org.bukkit.entity.Player;

public class Direction {

	public static String NORD = "N";
	public static String EST = "E";
	public static String SUD = "S";
	public static String OUEST = "O";
	public static String NULL = "NULL";	
	
	public static String getDirection(Player p) {
		
		float yaw = p.getLocation().getYaw();
		
		if(yaw < 0) yaw = 360 + yaw;
			
		if(yaw < 45 || yaw > 315) return Direction.SUD;
		else if(yaw > 45 && yaw < 135) return Direction.OUEST;
		else if(yaw > 135 && yaw < 225) return Direction.NORD;
		else if(yaw > 225 && yaw < 315) return Direction.EST;
		else return Direction.NULL;
	}
}
