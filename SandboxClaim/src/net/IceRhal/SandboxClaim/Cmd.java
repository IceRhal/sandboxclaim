package net.IceRhal.SandboxClaim;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import net.IceRhal.SandboxCore.j;
import net.minecraft.server.v1_8_R1.ChunkCoordIntPair;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.flags.InvalidFlagFormat;
import com.sk89q.worldguard.protection.flags.StateFlag.State;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class Cmd implements CommandExecutor {

	static HashMap<Player, ArrayList<ChunkCoordIntPair>> chunkList = new HashMap<Player, ArrayList<ChunkCoordIntPair>>();
	static HashMap<Player, BukkitTask> task = new HashMap<Player, BukkitTask>();
	static FileConfiguration players = Main.players;
	static FileConfiguration config = Main.config;
	static HashMap<Player, BlockVector[]> region = new HashMap<Player, BlockVector[]>();
	static ItemStack axe;
	static ArrayList<Player> chest = new ArrayList<Player>();
	WorldGuardPlugin w = WGBukkit.getPlugin();
	
	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		
		if(sender instanceof Player) {
			
			final Player p = (Player) sender;
			
			if(label.equalsIgnoreCase("claim") || label.equalsIgnoreCase("terrain")) {
				
				UUID id = p.getUniqueId();
				
				if(p.hasPermission("claim.user") || p.hasPermission("claim.admin")) {
					
					Location loc = p.getLocation();
					RegionManager rM = WGBukkit.getRegionManager(loc.getWorld());
					
					if(args.length > 0) {
						
						if(args[0].equalsIgnoreCase("aide") || args[0].equalsIgnoreCase("help") || args[0].equalsIgnoreCase("?")) {
							 
							ArrayList<String> help1 = new ArrayList<String>();
							ArrayList<String> help2 = new ArrayList<String>();
							ArrayList<String> help3 = new ArrayList<String>();
							ArrayList<String> help4 = new ArrayList<String>();
							
							help1.add(ChatColor.DARK_GREEN + "#### " + ChatColor.GOLD + "Aide Protection de Terrain Page 1/4" + ChatColor.DARK_GREEN + " ####");
							help1.add(ChatColor.GREEN + "/terrain " + ChatColor.GOLD + "Permet d'activer la visualisation de protection de terrain");
							help1.add(ChatColor.GREEN + "/terrain confirmer [nom] " + ChatColor.GOLD + "Permet de valider la protection d'un terrain en le nommant");
							help1.add(ChatColor.GREEN + "/terrain vider " + ChatColor.GOLD + "Permet de désactiver la visualisation de protection de terrain");
							help1.add(ChatColor.GREEN + "/terrain ajouter [pseudo] [terrain|*] " + ChatColor.GOLD + "Permet d'autorisé un joueur à construire sur un terrain ou tous tes terrains");
							
							help2.add(ChatColor.DARK_GREEN + "#### " + ChatColor.GOLD + "Aide Protection de Terrain Page 2/4" + ChatColor.DARK_GREEN + " ####");
							help2.add(ChatColor.GREEN + "/terrain enlever [pseudo] [terrain|*] " + ChatColor.GOLD + "Permet de retiré un joueur qui peut construire sur un ou tous tes terrain(s)");
							help2.add(ChatColor.GREEN + "/terrain liste [nb] " + ChatColor.GOLD + "Permet de voir la liste de ses terrains");
							help2.add(ChatColor.GREEN + "/terrain info [terrain] [nb] " + ChatColor.GOLD + "Permet d'avoir des infos sur un de ses terrains");
							help2.add(ChatColor.GREEN + "/terrain recharger " + ChatColor.GOLD + "Permet de recharger ses crédits pour éviter que ses claims expirent");
							help2.add(ChatColor.GREEN + "/terrain agrandir [nb] " + ChatColor.GOLD + "Permet d'agrandir la visualisation de terrain dans la direction où tu regarde");
														
							help3.add(ChatColor.DARK_GREEN + "#### " + ChatColor.GOLD + "Aide Protection de Terrain Page 3/4" + ChatColor.DARK_GREEN + " ####");
							help3.add(ChatColor.GREEN + "/terrain reduire [nb] " + ChatColor.GOLD + "Permet de réduire la visualisation de terrain dans la direction où tu regarde");
							help3.add(ChatColor.GREEN + "/terrain agrandirtous [nb] " + ChatColor.GOLD + "Permet d'agrandir la visualisation de terrain dans toutes les directions");
							help3.add(ChatColor.GREEN + "/terrain reduiretous [nb] " + ChatColor.GOLD + "Permet de réduire la visualisation de terrain dans toutes les directions");
							help3.add(ChatColor.GREEN + "/terrain selectionner [terrain] " + ChatColor.GOLD + "Permet de sélectionner un terrain (pour le redéfinir par exemple)");
							
							help4.add(ChatColor.DARK_GREEN + "#### " + ChatColor.GOLD + "Aide Protection de Terrain Page 4/4" + ChatColor.DARK_GREEN + " ####");
							help4.add(ChatColor.GREEN + "/terrain supprimer [terrain] " + ChatColor.GOLD + "Permet de supprimer un terrain à tout jamais");
							if(p.hasPermission("claim.admin")) help4.add(ChatColor.GREEN + "/aclaim help " + ChatColor.GOLD + "Permet d'afficher l'aide admin");
							help4.add(ChatColor.DARK_GREEN + "#### " + ChatColor.AQUA + "Plugin crée par " + ChatColor.BLUE + "IceRhal" + ChatColor.DARK_GREEN + " ####");
							
							if(args.length > 1) {
								
								try {
									
									int nb = Integer.parseInt(args[1]);
									
									if(nb == 2) {
										
										p.sendMessage(help2.toArray(new String[help2.size()]));
										return true;
									}
									else if(nb == 3) {
										
										p.sendMessage(help3.toArray(new String[help3.size()]));
										return true;
									}
									else if(nb > 3) {
										
										p.sendMessage(help4.toArray(new String[help4.size()]));
										return true;
									}
									
								} catch(NumberFormatException e) {}
							}
							
							p.sendMessage(help1.toArray(new String[help1.size()]));
							
						}
						else if(args[0].equalsIgnoreCase("expand") || args[0].equalsIgnoreCase("agrandir")) {

							if(!region.containsKey(p)) {
								
								p.sendMessage(ChatColor.RED + "Tu dois d'abord créer un terrain ou en sélectionner un pour ça.");
								return true;
							}
							
							int nb;
							
							if(args.length == 2) {
								
								if(Integer(args[1])) {
									
									nb = Integer.parseInt(args[1]);
									
									if(nb <= 0) {
									
										p.sendMessage(ChatColor.RED + "Le nombre doit être strictement positif /terrain agrandir [nombre]");
										return true;
									}				
									else {
										
										BlockVector min = region.get(p)[0];
										BlockVector max = region.get(p)[1];
										
										int y = p.getLocation().getBlockY() + 20;
										int minX = min.getBlockX();
										int minZ = min.getBlockZ();
										int maxX = max.getBlockX();
										int maxZ = max.getBlockZ();
										
										String dir = Direction.getDirection(p);
																				
										if(dir.equals(Direction.EST)) max = new BlockVector(maxX + nb, y, maxZ); //EST
										else if(dir.equals(Direction.NORD)) min = new BlockVector(minX, y, minZ - nb); //NORD
										else if(dir.equals(Direction.OUEST)) min = new BlockVector(minX - nb, y, minZ); //OUEST
										else if(dir.equals(Direction.SUD)) max = new BlockVector(maxX, y, maxZ + nb); //SUD
										else {
											
											p.sendMessage(ChatColor.RED + "Direction indéfinie.");
											return true;
										}
										
										if(isBig(p, min, max)) {
											
											p.sendMessage(ChatColor.RED + "Séléction trop grande.");
											return true;
										}
										
										resetView(p, min, max);
										p.sendMessage(ChatColor.GREEN + "Terrain agrandit de " + ChatColor.DARK_GREEN + nb 
												+ ChatColor.GREEN + " blocs vers là où tu regarde.");
									}
								}
								else {
									
									p.sendMessage(ChatColor.RED + "Erreur dans la commande /terrain agrandir [nombre]");
									return true;
								}							
							}						
						}
						else if(args[0].equalsIgnoreCase("expandall") || args[0].equalsIgnoreCase("agrandirtous")) {
							
							if(!region.containsKey(p)) {
								
								p.sendMessage(ChatColor.RED + "Tu dois d'abord créer un terrain ou en sélectionner un pour ça.");
								return true;
							}
							
							int nb;
							
							if(args.length == 2) {
								
								if(Integer(args[1])) {
									
									nb = Integer.parseInt(args[1]);
									
									if(nb <= 0) {
									
										p.sendMessage(ChatColor.RED + "Le nombre doit être strictement positif." + ChatColor.DARK_RED + " /terrain agrandir [nombre]");
										return true;
									}				
									else {
										
										BlockVector min = region.get(p)[0];
										BlockVector max = region.get(p)[1];
										
										int y = p.getLocation().getBlockY() + 20;
										int minX = min.getBlockX();
										int minZ = min.getBlockZ();
										int maxX = max.getBlockX();
										int maxZ = max.getBlockZ();
										
										max = new BlockVector(maxX + nb, y, maxZ); //EST
										min = new BlockVector(minX, y, minZ - nb); //NORD
										min = new BlockVector(minX - nb, y, minZ); //OUEST
										max = new BlockVector(maxX, y, maxZ + nb); //SUD
										
										if(isBig(p, min, max)) {
											
											p.sendMessage(ChatColor.RED + "Séléction trop grande.");
											return true;
										}
										
										resetView(p, min, max);
										p.sendMessage(ChatColor.GREEN + "Terrain agrandit de " + ChatColor.DARK_GREEN + nb 
												+ ChatColor.GREEN + " blocs de tous les côtés");
									}
								}
								else {
									
									p.sendMessage(ChatColor.RED + "Erreur dans la commande /terrain agrandirtous [nombre]");
									return true;
								}							
							}
							
						}
						else if(args[0].equalsIgnoreCase("contract") || args[0].equalsIgnoreCase("reduire")) {	
							
							if(!region.containsKey(p)) {
								
								p.sendMessage(ChatColor.RED + "Tu dois d'abord créer un terrain ou en sélectionner un pour ça.");
								return true;
							}

							int nb;
							
							if(args.length == 2) {
								
								if(Integer(args[1])) {
									
									nb = Integer.parseInt(args[1]);
									
									if(nb <= 0) {
									
										p.sendMessage(ChatColor.RED + "Le nombre doit être strictement positif /terrain reduire [nombre]");
										return true;
									}				
									else {
										
										BlockVector min = region.get(p)[0];
										BlockVector max = region.get(p)[1];
										
										int y = p.getLocation().getBlockY() + 20;
										int minX = min.getBlockX();
										int minZ = min.getBlockZ();
										int maxX = max.getBlockX();
										int maxZ = max.getBlockZ();
										
										String dir = Direction.getDirection(p);
										
										if(dir.equals(Direction.EST)) max = new BlockVector(maxX - nb, y, maxZ); //EST
										else if(dir.equals(Direction.NORD)) min = new BlockVector(minX, y, minZ + nb); //NORD
										else if(dir.equals(Direction.OUEST)) min = new BlockVector(minX + nb, y, minZ); //OUEST
										else if(dir.equals(Direction.SUD)) max = new BlockVector(maxX, y, maxZ - nb); //SUD
										else {
											
											p.sendMessage(ChatColor.RED + "Direction indéfinie.");
											return true;
										}
										
										if(isBig(p, min, max)) {
											
											p.sendMessage(ChatColor.RED + "Séléction trop grande.");
											return true;
										}
										
										resetView(p, min, max);
										p.sendMessage(ChatColor.GREEN + "Terrain réduit de " + ChatColor.DARK_GREEN + nb 
												+ ChatColor.GREEN + " blocs de la direction où tu regarde.");
									}
								}
								else {
									
									p.sendMessage(ChatColor.RED + "Erreur dans la commande /terrain reduire [nombre]");
									return true;
								}							
							}						
						}
						else if(args[0].equalsIgnoreCase("contractall") || args[0].equalsIgnoreCase("reduiretous")) {	
							
							if(!region.containsKey(p)) {
								
								p.sendMessage(ChatColor.RED + "Tu dois d'abord créer un terrain ou en sélectionner un pour ça.");
								return true;
							}

							int nb;
							
							if(args.length == 2) {
								
								if(Integer(args[1])) {
									
									nb = Integer.parseInt(args[1]);
									
									if(nb <= 0) {
									
										p.sendMessage(ChatColor.RED + "Le nombre doit être strictement positif /terrain reduiretous [nombre]");
										return true;
									}				
									else {
										
										BlockVector min = region.get(p)[0];
										BlockVector max = region.get(p)[1];
										
										int y = p.getLocation().getBlockY() + 20;
										int minX = min.getBlockX();
										int minZ = min.getBlockZ();
										int maxX = max.getBlockX();
										int maxZ = max.getBlockZ();
																				
										max = new BlockVector(maxX - nb, y, maxZ);
										min = new BlockVector(minX, y, minZ + nb);
										min = new BlockVector(minX + nb, y, minZ);
										max = new BlockVector(maxX, y, maxZ - nb);
										
										if(isBig(p, min, max)) {
											
											p.sendMessage(ChatColor.RED + "Séléction trop grande.");
											return true;
										}
										
										resetView(p, min, max);
										p.sendMessage(ChatColor.GREEN + "Terrain réduit de " + ChatColor.DARK_GREEN + nb 
												+ ChatColor.GREEN + " blocs dans toutes les directions.");
									}
								}
								else {
									
									p.sendMessage(ChatColor.RED + "Erreur dans la commande /terrain reduiretous [nombre]");
									return true;
								}							
							}						
						}
						else if(args[0].equalsIgnoreCase("confirm") || args[0].equalsIgnoreCase("confirmer")) {
							
							if(!region.containsKey(p)) {
								
								p.sendMessage(ChatColor.RED + "Tu dois d'abord crée un terrain ou en sélectionner un pour ça.");
								return true;
							}
							
							if(args.length == 2) {
								
								if(config.contains("off_worlds")) {
									
									for(String str : config.getStringList("off_worlds")) {
										
										if(loc.getWorld().getName().equalsIgnoreCase(str)) {
											
											p.sendMessage(ChatColor.RED + "Les protections de terrains dans ce monde sont désactivé.");
											return true;
										}
									}
								}
								
								int limit = Main.getLimit(p.getName(), p.getWorld());
								
								if(limit > 0) {
									
									limit -= Main.getBlocs(p.getName());
									
									if(limit < 0) {
										
										p.sendMessage(ChatColor.RED + "Ta limite de terrains (" + ChatColor.DARK_RED + Main.getLimit(p.getName(), p.getWorld()) + ChatColor.RED 
												+ " bloc(s)) est déjà atteinte. Tu ne peux donc pas faire un autre terrain.");
										return true;
									}
									
									int peri = (region.get(p)[1].getBlockX() - region.get(p)[0].getBlockX() + 1) * (region.get(p)[1].getBlockZ() - region.get(p)[0].getBlockZ() + 1);
									limit -= peri;
									
									if(limit < 0) {
										
										p.sendMessage(ChatColor.RED + "Tu ne peux pas crée ce terrain car tu dépasse ta limite de terrains (" + ChatColor.DARK_RED + Main.getLimit(p.getName(), p.getWorld()) + ChatColor.RED
												+ " bloc(s)) de : " + ChatColor.DARK_RED + (-limit) + ChatColor.RED + " blocs.");
										return true;
									}
								}
								
								if(args[1].matches("[^A-Za-z0-9-_]")) {
									
									p.sendMessage(ChatColor.RED + "Nom de terrain invalide. Seul les lettres, chiffres, underscores, ou tirets sont acceptés");
									return true;
								}
								
								ProtectedRegion r;
								
								try {
									
									r = new ProtectedCuboidRegion("claim_" + id + "_" + args[1].toLowerCase(), region.get(p)[0], region.get(p)[1]);
								
								} catch(IllegalArgumentException e) {
									
									p.sendMessage(ChatColor.RED + "Nom de terrain invalide. Seul les lettres, chiffres, underscores, ou tirets sont acceptés");
									return true;
								}
								
								Iterator<ProtectedRegion> it = rM.getApplicableRegions(r).getRegions().iterator();
								ArrayList<ProtectedRegion> regs = new ArrayList<ProtectedRegion>();
								
								while(it.hasNext()) {
									
									ProtectedRegion reg = it.next();
									
									if(reg.getId().equalsIgnoreCase(r.getId())) continue;
									
									if(!reg.getFlags().containsKey(Main.claimable) || 
											(reg.getFlags().containsKey(Main.claimable)
											&& reg.getFlag(Main.claimable) != State.ALLOW)) regs.add(reg);
								}
								
								if(regs.isEmpty()) {
																									
									DefaultDomain d = new DefaultDomain();
									
									d.addPlayer(p.getUniqueId());
									
									r.setOwners(d);
									rM.addRegion(r);
									
									try {
										
										r.setFlag(DefaultFlag.GREET_MESSAGE, DefaultFlag.GREET_MESSAGE.parseInput(WGBukkit.getPlugin(), 
												sender, ChatColor.AQUA + "Tu entres sur le terrain : " + ChatColor.DARK_AQUA + args[1].toUpperCase().substring(0, 1) 
												+ args[1].substring(1).toLowerCase() + ChatColor.AQUA + " de " + ChatColor.DARK_AQUA + p.getName()));
										
										r.setFlag(DefaultFlag.FAREWELL_MESSAGE, DefaultFlag.FAREWELL_MESSAGE.parseInput(WGBukkit.getPlugin(), 
												sender, ChatColor.LIGHT_PURPLE + "Tu sors du terrain : " + ChatColor.DARK_PURPLE + args[1].toUpperCase().substring(0, 1) 
												+ args[1].substring(1).toLowerCase() + ChatColor.LIGHT_PURPLE + " de " + ChatColor.DARK_PURPLE + p.getName()));
										
										r.setFlag(DefaultFlag.ENDERDRAGON_BLOCK_DAMAGE, State.DENY);
										r.setFlag(DefaultFlag.FIRE_SPREAD, State.DENY);
										r.setFlag(j.as, State.DENY);
										r.setFlag(j.at, State.DENY);
										r.setFlag(Main.claimable, State.DENY);
									} catch (InvalidFlagFormat e) {}
																		
									delView(p, true);							
									
									p.sendMessage(ChatColor.AQUA + "Tu viens de protéger le terrain : " + ChatColor.DARK_AQUA + args[1].toLowerCase());
									
									if(!players.contains(id + "")) {
										
										players.set(id + "", Main.getExpireBlocs(p.getName()));
										Main.cM.f("players.yml");
										p.sendMessage(ChatColor.GREEN + "C'est ton premier terrain donc il est gratuit pour 1 semaine.");
									}
									
									if(Main.getExpireTime(id) > 60 * 24 * 60) {
										
										players.set(id + "", Main.getExpireBlocs(p.getName()) * 8);										
										Main.cM.f("players.yml");
									}
									
									int min = (int) Main.getExpireTime(id);
									int h = min / 60;
									int j = h / 24;
									
									String time = ChatColor.GOLD + "";
									
									if(min < 60) time += min + "" + ChatColor.YELLOW + " minute(s)";
									else if(h < 24) time += h + "" + ChatColor.YELLOW + " heure(s)";
									else time += j + "" + ChatColor.YELLOW + " jour(s)";
									
									p.sendMessage(ChatColor.YELLOW + "Tes terrains expirent dans " + time + ".");
									
									p.sendMessage(ChatColor.RED + "Oublie pas de recharger tes terrains avec du blé.");
									p.sendMessage(ChatColor.RED + "La commande : " + ChatColor.DARK_RED + "/terrain recharge");
									
									if(Main.getLimit(p.getName(), p.getWorld()) >= 0) p.sendMessage(ChatColor.DARK_GREEN + "" + Main.getBlocs(p.getName()) 
											+ ChatColor.GREEN + " bloc(s) protégé(s) sur les " + ChatColor.DARK_GREEN 
											+ Main.getLimit(p.getName(), p.getWorld()) + ChatColor.GREEN + " autorisés");
								}
								else {
									
									p.sendMessage(ChatColor.RED + "Tu ne peux pas protéger cette zone car un terrain est déjà présent.");
								}
							}
							else p.sendMessage(ChatColor.RED + "Erreur dans la commande /terrain confirmer [nom]");

						}
						else if(args[0].equalsIgnoreCase("select") || args[0].equalsIgnoreCase("selectionner")) {
							
							if(args.length == 2) {
								
								ProtectedRegion r = rM.getRegion("claim_" + id + "_" + args[1].toLowerCase());
								
								if(r != null) {
									
									resetView(p, r.getMinimumPoint(), r.getMaximumPoint());
									p.sendMessage(ChatColor.GREEN + "Terrain séléctionné.");
								}
								else p.sendMessage(ChatColor.RED + "Terrain inconnue.");					
							}
							else p.sendMessage(ChatColor.RED + "Erreur dans la commande /terrain selectionner [nom]");
						}
						else if(args[0].equalsIgnoreCase("add") || args[0].equalsIgnoreCase("ajouter")) {
							
							if(args.length == 3) {
								
								OfflinePlayer p2 = net.IceRhal.SandboxCore.Main.h(args[1]);
														
								if(p2 != null) {
									
									LocalPlayer p1 = WGBukkit.getPlugin().wrapPlayer(p);
									LocalPlayer p3 = WGBukkit.getPlugin().wrapOfflinePlayer(p2);
											
									if(p2.getUniqueId() == p.getUniqueId()) {
										
										p.sendMessage(ChatColor.RED + "Tu ne peux pas t'ajouter comme membre sur tes terrains.");
										return true;
									}
									
									if(args[2].equalsIgnoreCase("*")) {
										
										for(ProtectedRegion r : rM.getRegions().values()) {
											
											if(r.getOwners().contains(p1)) {

												DefaultDomain d = r.getMembers();
												
												d.addPlayer(p3);
												
												r.setMembers(d);
											}
										}
										
										p.sendMessage(ChatColor.GREEN + "Le joueur " + ChatColor.DARK_GREEN + p2.getName() + ChatColor.GREEN
												+ " a été ajouté à tout tes terrains.");
									}
									else {
										
										ProtectedRegion r = rM.getRegion("claim_" + id + "_" + args[2].toLowerCase());
										
										if(r != null) {
											
											DefaultDomain d = r.getMembers();
											
											if(!d.contains(p3)) {
												
												d.addPlayer(p3);
												
												r.setMembers(d);
												
												p.sendMessage(ChatColor.GREEN + "Le joueur " + ChatColor.DARK_GREEN + p2.getName() + ChatColor.GREEN
														+ " a été ajoute au terrain " + ChatColor.DARK_GREEN + args[2]);
											}
											else p.sendMessage(ChatColor.RED + "Le joueur " + ChatColor.DARK_RED + p2.getName() + ChatColor.RED 
													+ " est déjà présent sur ce terrain.");				
										}
										else p.sendMessage(ChatColor.RED + "Terrain inconnu.");
									}
								}
								else p.sendMessage(ChatColor.RED + "Joueur " + ChatColor.DARK_RED + args[1] + ChatColor.RED + " introuvable.");
							}
							else p.sendMessage(ChatColor.RED + "Erreur dans la commande /terrain ajouter [pseudo] [terrain|*]");
						}
						else if(args[0].equalsIgnoreCase("remove") || args[0].equalsIgnoreCase("enlever")) {
							
							if(args.length == 3) {
								
								OfflinePlayer p2 = Bukkit.getOfflinePlayer(args[1]);
								
								if(p2 != null) {
									
									LocalPlayer p1 = WGBukkit.getPlugin().wrapPlayer(p);
									LocalPlayer p3 = WGBukkit.getPlugin().wrapOfflinePlayer(p2);
									
									if(args[2].equalsIgnoreCase("*")) {
										
										for(RegionManager rM2: WGBukkit.getPlugin().getRegionContainer().getLoaded()) {
											
											for(ProtectedRegion r : rM2.getRegions().values()) {
												
												if(r.getOwners().contains(p1)) {

													DefaultDomain d = r.getMembers();
													
													if(d.contains(p3)) d.removePlayer(p3);
													
													r.setMembers(d);
												}
											}
										}
										
										p.sendMessage(ChatColor.GREEN + "Le joueur " + ChatColor.DARK_GREEN + p2.getName() + ChatColor.GREEN
												+ " a été retiré à tout tes terrains.");
									}
									else {
										
										ProtectedRegion r = rM.getRegion("claim_" + id + "_" + args[2].toLowerCase());
										
										if(r != null) {
											
											DefaultDomain d = r.getMembers();
											
											if(d.contains(p3)) {
												
												d.removePlayer(p3);
												
												r.setMembers(d);
												
												p.sendMessage(ChatColor.GREEN + "Le joueur " + ChatColor.DARK_GREEN + p2.getName() + ChatColor.GREEN
														+ " a été enlevé du terrain " + ChatColor.DARK_GREEN + args[2]);
											}
											else p.sendMessage(ChatColor.RED + "Le joueur " + ChatColor.DARK_RED + p2.getName() + ChatColor.RED 
													+ " n'est déjà pas présent sur ce terrain.");									
										}
										else p.sendMessage(ChatColor.RED + "Terrain inconnu.");
									}									
								}
								else p.sendMessage(ChatColor.RED + "Joueur " + ChatColor.DARK_RED + args[1] + ChatColor.RED + " introuvable.");
							}
							else p.sendMessage(ChatColor.RED + "Erreur dans la commande /terrain enlever [pseudo] [terrain|*]");
						}
						else if(args[0].equalsIgnoreCase("delete") || args[0].equalsIgnoreCase("supprimer")) {
							
							if(args.length == 2) {
								
								if(args[1].equalsIgnoreCase("*")) {
									
									int nb = 0;
									
									for(RegionManager rM2 : WGBukkit.getPlugin().getRegionContainer().getLoaded()) {
										
										for(ProtectedRegion r : rM2.getRegions().values()) {
											
											if(r.getOwners().contains(p.getUniqueId())) {

												rM2.removeRegion(r.getId());
												nb++;
											}
										}
									}
									
									if(nb > 0) {
										
										p.sendMessage(ChatColor.GREEN + "Tous tes terrains sont supprimés (" + ChatColor.DARK_GREEN + nb + ChatColor.GREEN + " terrain(s)).");
									}
									else p.sendMessage(ChatColor.GREEN + "Tu posséde déjà aucun terrain.");
									
									return true;
								}
								
								ProtectedRegion r = rM.getRegion("claim_" + id + "_" + args[1].toLowerCase());
								
								if(r != null) {
									
									rM.removeRegion(r.getId());
									
									p.sendMessage(ChatColor.GREEN + "Terrain " + ChatColor.DARK_GREEN + args[1] + ChatColor.GREEN + " supprimé.");
								}
								else p.sendMessage(ChatColor.RED + "Terrain inconnu.");
							}
							else p.sendMessage(ChatColor.RED + "Erreur dans la commande /terrain supprimer [terrain|*]");
						}
						else if(args[0].equalsIgnoreCase("list") || args[0].equalsIgnoreCase("liste")) {
							
							HashMap<ProtectedRegion, String> own = new HashMap<ProtectedRegion, String>();
							HashMap<ProtectedRegion, String> members = new HashMap<ProtectedRegion, String>();
							
							int page = 1;
							
							if(args.length >= 2) {
								
								try {
									
									page = Integer.parseInt(args[1]);
									
									if(page <= 0) page = 1;
								} catch(NumberFormatException e) {
									
									page = 1;
								}
							}	

							ArrayList<String> mess = new ArrayList<String>();							
							LocalPlayer p1 = WGBukkit.getPlugin().wrapPlayer(p);
							
							for(RegionManager rM2 : WGBukkit.getPlugin().getRegionContainer().getLoaded()) {
								
								for(ProtectedRegion r : rM2.getRegions().values()) {
									
									if(!(r.getId().startsWith("claim_") && r.getId().length() >= 43)) continue;
									
									if(r.isOwner(p1)) own.put(r, rM2.getName());
									else if(r.isMember(p1)) members.put(r, rM2.getName());
								}
							}
							
							mess.add(ChatColor.GREEN + "Voici la liste des terrains où tu peux construire.");
							mess.add(ChatColor.GREEN + "En gris ce sont les terrains où tu es juste allow.");
							
							int min = (int) Main.getExpireTime(id);
							int h = min / 60;
							int j = h / 24;
							
							String time = ChatColor.DARK_PURPLE + "";
							
							if(min < 60) time += min + "" + ChatColor.LIGHT_PURPLE + " minute(s)";
							else if(h < 24) {
								
								if(min % 60 == 0) time += h + "" + ChatColor.LIGHT_PURPLE + " heure(s)";
								else time += h + "" + ChatColor.LIGHT_PURPLE + " heure(s) et " + ChatColor.DARK_PURPLE + min % 60 + ChatColor.LIGHT_PURPLE + " minute(s)";
							}
							else {
								
								if(h % 24 == 0) time += j + "" + ChatColor.LIGHT_PURPLE + " jour(s)";
								else time += j + "" + ChatColor.LIGHT_PURPLE + " jour(s) et " + ChatColor.DARK_PURPLE + h % 24 + ChatColor.LIGHT_PURPLE + " heure(s)";
							}
													
							if(Main.getBlocs(p.getName()) > 0) mess.add(ChatColor.LIGHT_PURPLE + "Tes terrains vont expirer dans : " + time + ".");
							
							if(Main.getLimit(p.getName(), p.getWorld()) >= 0) mess.add(ChatColor.DARK_GREEN + "" + Main.getBlocs(p.getName()) 
									+ ChatColor.GREEN + " bloc(s) protégé(s) sur les " + ChatColor.DARK_GREEN 
									+ Main.getLimit(p.getName(), p.getWorld()) + ChatColor.GREEN + " autorisés");
							else mess.add(ChatColor.DARK_GREEN + "" + Main.getBlocs(p.getName()) 
									+ ChatColor.GREEN + " bloc(s) protégé(s)");
																					
							for(ProtectedRegion r : own.keySet()) {
								
								int x = (r.getMinimumPoint().getBlockX() + r.getMaximumPoint().getBlockX()) / 2;
								int z = (r.getMinimumPoint().getBlockZ() + r.getMaximumPoint().getBlockZ()) / 2;;
															
								mess.add(ChatColor.AQUA + "- " + ChatColor.BLUE + r.getId().substring(43) + ChatColor.AQUA 
										+ " dans " + ChatColor.DARK_AQUA + own.get(r) + ChatColor.AQUA + " aux coordonnées : " + ChatColor.DARK_AQUA + x
										+ ChatColor.AQUA + ", " + ChatColor.DARK_AQUA + z);
							}
							
							for(ProtectedRegion r : members.keySet()) {
								
								int x = (r.getMinimumPoint().getBlockX() + r.getMaximumPoint().getBlockX()) / 2;
								int z = (r.getMinimumPoint().getBlockZ() + r.getMaximumPoint().getBlockZ()) / 2;;
								
								mess.add(ChatColor.AQUA + "- " + ChatColor.GRAY + r.getId().substring(43) + ChatColor.AQUA + " de " 
										+ ChatColor.DARK_AQUA + Bukkit.getOfflinePlayer(UUID.fromString(r.getId().substring(6, 42))).getName() + ChatColor.AQUA
										+ " dans " + ChatColor.DARK_AQUA + members.get(r) + ChatColor.AQUA + " aux coordonnées : " + ChatColor.DARK_AQUA + x
										+ ChatColor.AQUA + ", " + ChatColor.DARK_AQUA + z);
							}
							
							int pmax = mess.size() / 9;
							if(mess.size() % 9 != 0) pmax++;
							
							if(page > pmax) page = pmax;
							
							p.sendMessage(ChatColor.YELLOW + "######### " + ChatColor.GOLD + "Liste des Terrains (Page " + page + "/" + pmax 
									+ ")" + ChatColor.YELLOW + " ##########");
							
							page--;
							
							for(int x = 0; x < 9; x++) {
								
								if((x + (page * 9)) < mess.size()) p.sendMessage(mess.get(x + (page * 9)));
							}
						}
						else if(args[0].equalsIgnoreCase("clear") || args[0].equalsIgnoreCase("vider")) {
							
							if(!region.containsKey(p)) {
								
								p.sendMessage(ChatColor.RED + "Tu dois d'abord crée un terrain ou en sélectionner un pour ça.");
								return true;
							}
							
							if(Events.sel.containsKey(p)) Events.sel.remove(p);
							
							delView(p, true);
							p.sendMessage(ChatColor.GREEN + "Visualisation des terrains vidés");
						}
						else if(args[0].equalsIgnoreCase("recharge") || args[0].equalsIgnoreCase("recharger")) {
							
							p.sendMessage(ChatColor.GREEN + "Pour recharger tes terrains, met du blé dans cet inventaire.");
							
							int b = (int) Main.getExpireBlocs(p.getName());
							if(b > 0) p.sendMessage(ChatColor.AQUA + "Pour recharger pour " + ChatColor.DARK_AQUA + "1" + ChatColor.AQUA + 
									" jour il te faut " + ChatColor.DARK_AQUA + b / 7 + ChatColor.AQUA + " blé.");
							p.openInventory(Bukkit.createInventory(p, 36, ChatColor.BOLD + "Interface de Rechargement"));
						}
						else if(args[0].equalsIgnoreCase("info")) {
							
							if(args.length >= 2) {
							
								ProtectedRegion r = null;
								String world = "world";
								
								for(RegionManager rM2 : WGBukkit.getPlugin().getRegionContainer().getLoaded()) {
									
									for(ProtectedRegion reg : rM2.getRegions().values()) {
										
										if(reg.isOwner(WGBukkit.getPlugin().wrapPlayer(p)) 
												&& reg.getId().equalsIgnoreCase("claim_" + p.getUniqueId() + "_" + args[1])) {
											
											r = reg;
											world = rM2.getName();
											break;
										}
									}
								}
								
								if(r == null) {
									
									p.sendMessage(ChatColor.RED + "Terrain introuvable.");
									return true;
								}
								
								ArrayList<String> mess = new ArrayList<String>();
								
								mess.add(ChatColor.GREEN + "Nom du Terrain : " + ChatColor.DARK_GREEN 
										+ args[1].substring(0, 1).toUpperCase() + args[1].substring(1).toLowerCase());
								
								mess.add(ChatColor.GREEN + "Propriétaire : " + ChatColor.DARK_GREEN + p.getName());
								
								mess.add(ChatColor.GREEN + "Surface : " + ChatColor.DARK_GREEN + ((r.getMaximumPoint().getBlockX() 
										- r.getMinimumPoint().getBlockX() + 1) * (r.getMaximumPoint().getBlockZ() 
												- r.getMinimumPoint().getBlockZ() + 1)) + ChatColor.GREEN + " cube(s).");
								int x = (r.getMaximumPoint().getBlockX() + r.getMinimumPoint().getBlockX()) / 2;
								int z = (r.getMaximumPoint().getBlockZ() + r.getMinimumPoint().getBlockZ()) / 2;
								
								mess.add(ChatColor.GREEN + "Position : monde " + ChatColor.DARK_GREEN + world + ChatColor.GREEN 
										+ ", x : " + ChatColor.DARK_GREEN + x + ChatColor.GREEN + ", z : " 
										+ ChatColor.DARK_GREEN + z); 
								if(r.getMembers().size() <= 1) {
									
									mess.add(ChatColor.GREEN + "Membre (" + ChatColor.DARK_GREEN + r.getMembers().size() + ChatColor.GREEN + ")");
								}
								else mess.add(ChatColor.GREEN + "Membres (" + ChatColor.DARK_GREEN + r.getMembers().size() + ChatColor.GREEN + ")");
								
								for(UUID id2 : r.getMembers().getUniqueIds()) {
									
									mess.add(ChatColor.AQUA + " - " + Bukkit.getOfflinePlayer(id2).getName());
								}								
																
								int page = 1;
								
								if(args.length >= 3) {
									
									try {
										
										page = Integer.parseInt(args[2]);
										
										if(page <= 0) page = 1;
									} catch(NumberFormatException e) {
										
										page = 1;
									}
								}	
								
								int pmax = mess.size() / 9;
								if(mess.size() % 9 != 0) pmax++;
								
								if(page > pmax) page = pmax;
								
								p.sendMessage(ChatColor.YELLOW + "######### " + ChatColor.GOLD + "Info des Terrains (Page " + page + "/" + pmax 
										+ ")" + ChatColor.YELLOW + " ##########");
								
								page--;
								
								for(x = 0; x < 9; x++) {
									
									if((x + (page * 9)) < mess.size()) p.sendMessage(mess.get(x + (page * 9)));
								}
							}
							else p.sendMessage(ChatColor.RED + "Erreur dans la commande. (" + ChatColor.DARK_RED + "/terrain info [terrain] [nb]" + ChatColor.RED + ").");							
						}
						else p.sendMessage(ChatColor.RED + "Erreur dans la commande. Aide : " + ChatColor.DARK_RED + "/terrain aide");
					}
					else {
						
						int x = loc.getBlockX();
						int z = loc.getBlockZ();
						
						BlockVector min = new BlockVector(x - 10, 30, z - 10); //Nord-Ouest
						BlockVector max = new BlockVector(x + 10, 255, z + 10); //Sud-Est
						
						if(isBig(p, min, max)) {
							
							p.sendMessage(ChatColor.RED + "Séléction trop grande.");
							return true;
						}
						
						resetView(p, min, max);
						
						BlockVector[] bV = new BlockVector[2];
						
						bV[0] = min;
						bV[1] = max;
						
						Events.sel.put(p, bV);
						
						p.sendMessage(ChatColor.AQUA + "Tu viens d'activer la vue de protection de terrain (= une protection) d'un carré de " +
								"21 X 21 blocs centrés sur toi (les blocs en verres au dessus de toi).");

						boolean hasAxe = false;
						for(ItemStack is : p.getInventory().getContents()) {
							
							if(is != null && is.getType() == Material.GOLD_AXE) {
								
								hasAxe = true;
								break;
							}
						}
						
						if(!hasAxe) p.getInventory().addItem(axe);
						p.sendMessage(ChatColor.YELLOW + "Pour agrandir ou rétrécir ton terrain utilise une hache en or (clique droit et clique gauche).");
						
						p.sendMessage(ChatColor.GREEN + "Fais " + ChatColor.DARK_GREEN + "/terrain confirmer [nom]" 
								+ ChatColor.GREEN + " pour valider ton terrain et le nommer.");
						
						if(Main.getLimit(p.getName(), p.getWorld()) >= 0) p.sendMessage(ChatColor.DARK_GREEN + "" + Main.getBlocs(p.getName()) 
								+ ChatColor.GREEN + " bloc(s) terrain sur les " + ChatColor.DARK_GREEN 
								+ Main.getLimit(p.getName(), p.getWorld()) + ChatColor.GREEN + " autorisés");
						
						p.sendMessage(ChatColor.AQUA + "Pour vider ta séléction : " + ChatColor.DARK_AQUA + "/terrain vider");
					}
				}
				else p.sendMessage(noperm("claim.user"));
				
				return true;
			}
			else if(label.equalsIgnoreCase("adminclaim") || label.equalsIgnoreCase("aclaim")) {
				
				if(p.hasPermission("claim.admin")) {
					
					RegionManager rM = WGBukkit.getRegionManager(p.getWorld());
					
					if(args.length == 0 || args[0].equalsIgnoreCase("help") || args[0].equalsIgnoreCase("?")) {
						
						ArrayList<String> help = new ArrayList<String>();
						
						help.add(ChatColor.DARK_GREEN + "#### " + ChatColor.GOLD + "SandboxClaim - Admin Help" 
									+ ChatColor.DARK_GREEN + " ####");
						help.add(ChatColor.GREEN + "/aclaim help " + ChatColor.GOLD + "Affiche l'aide admin");
						help.add(ChatColor.GREEN + "/aclaim list [pseudo] " + ChatColor.GOLD + "Permet de voir les claims d'un joueur");
						help.add(ChatColor.GREEN + "/aclaim info [pseudo] [claim] " + ChatColor.GOLD + "Permet d'avoir des infos sur le claim d'un joueur"); 
						if(p.hasPermission("claim.delete")) help.add(ChatColor.GREEN + "/aclaim delete [pseudo] [claim|*] " + ChatColor.GOLD + "Permet de supprimer un claim à un joueur (ou tous ses claims)");
						if(p.hasPermission("claim.addcredit")) help.add(ChatColor.GREEN + "/aclaim addcredit [pseudo] [nb] " + ChatColor.GOLD + "Permet d'ajouter des crédits à un joueur");
						if(p.hasPermission("claim.setchest")) help.add(ChatColor.GREEN + "/aclaim setchest " + ChatColor.GOLD + "Permet de définir l'arrivée des crédits");
							
						p.sendMessage(help.toArray(new String[help.size()]));
						
						return true;
					}
					else if(args[0].equalsIgnoreCase("delete") && p.hasPermission("claim.delete")) {
						
						if(args.length == 3) {							

							OfflinePlayer p2 = net.IceRhal.SandboxCore.Main.h(args[1]);
							
							if(p2 == null || !p2.hasPlayedBefore()) {
								
								p.sendMessage(ChatColor.RED + "Joueur " + ChatColor.DARK_RED + args[1] + ChatColor.RED + " introuvable.");
								return true;
								
							}							
							
							if(args[2].equalsIgnoreCase("*")) {
								
								int nb = 0;
								
								for(RegionManager rM2 : WGBukkit.getPlugin().getRegionContainer().getLoaded()) {
									
									for(ProtectedRegion r : rM2.getRegions().values()) {
										
										if(r.getOwners().contains(p2.getName())) {

											rM2.removeRegion(r.getId());
											nb++;
										}
									}
								}
								
								if(nb > 0) {
									
									p.sendMessage(ChatColor.GREEN + "Tous les terrains de " + ChatColor.DARK_GREEN + p2.getName() 
											+ ChatColor.GREEN + " sont supprimés (" + ChatColor.DARK_GREEN + nb + ChatColor.GREEN + " terrain(s)).");
								}
								else p.sendMessage(ChatColor.GREEN + "Il posséde déjà aucun terrain.");
								
								return true;
							}
							
							ProtectedRegion r = rM.getRegion("claim_" + p2.getUniqueId() + "_" + args[2].toLowerCase());
							
							if(r != null) {
								
								rM.removeRegion(r.getId());
								
								p.sendMessage(ChatColor.GREEN + "Claim " + ChatColor.DARK_GREEN + args[2] + ChatColor.GREEN + " supprimé.");
							}
							else p.sendMessage(ChatColor.RED + "Claim inconnu.");
						}
						else p.sendMessage(ChatColor.RED + "Erreur dans la commande. " + ChatColor.DARK_RED + "/aclaim delete [pseudo] [claim|*]");
					}
					else if(args[0].equalsIgnoreCase("list")) {
						
						if(args.length >= 2) {
							
							OfflinePlayer p2 = net.IceRhal.SandboxCore.Main.h(args[1]);
							
							if(p2 == null || !p2.hasPlayedBefore()) {
								
								p.sendMessage(ChatColor.RED + "Joueur " + ChatColor.DARK_RED + args[1] + ChatColor.RED + " introuvable.");
								return true;
							}
							
							int page = 1;
							
							if(args.length >= 3) {
								
								try {
									
									page = Integer.parseInt(args[2]);
									
									if(page <= 0) page = 1;
								} catch(NumberFormatException e) {
									
									page = 1;
								}
							}	
							
							LocalPlayer p1 = WGBukkit.getPlugin().wrapOfflinePlayer(p2);

							ArrayList<String> mess = new ArrayList<String>();
							
							HashMap<ProtectedRegion, String> own = new HashMap<ProtectedRegion, String>();
							HashMap<ProtectedRegion, String> members = new HashMap<ProtectedRegion, String>();
							
							for(RegionManager rM2 : WGBukkit.getPlugin().getRegionContainer().getLoaded()) {
								
								for(ProtectedRegion r : rM2.getRegions().values()) {
									
									if(!(r.getId().startsWith("claim_") && r.getId().length() >= 43)) continue;
									
									if(r.isOwner(p1)) own.put(r, rM2.getName());
									else if(r.isMember(p1)) members.put(r, rM2.getName());
								}
							}
							
							mess.add(ChatColor.GREEN + "Voici la liste des claims où " + ChatColor.DARK_GREEN + p2.getName() + ChatColor.GREEN + " peux construire.");
							mess.add(ChatColor.GREEN + "En gris ce sont les claims où il est juste allow.");
							
							int min = (int) Main.getExpireTime(p2.getUniqueId());
							int h = min / 60;
							int j = h / 24;
							
							String time = ChatColor.DARK_PURPLE + "";
							
							if(min < 60) time += min + "" + ChatColor.LIGHT_PURPLE + " minute(s)";
							else if(h < 24) time += h + "" + ChatColor.LIGHT_PURPLE + " heure(s)";
							else time += j + "" + ChatColor.LIGHT_PURPLE + " jour(s)";
							
							if(Main.getBlocs(p2.getName()) > 0) mess.add(ChatColor.LIGHT_PURPLE + "Ses claims vont expirer dans : " + time + ".");
							if(Main.getLimit(p2.getName(), p.getWorld()) >= 0) mess.add(ChatColor.DARK_GREEN + "" + Main.getBlocs(p2.getName()) 
									+ ChatColor.GREEN + " bloc(s) claim sur les " + ChatColor.DARK_GREEN 
									+ Main.getLimit(p2.getName(), p.getWorld()) + ChatColor.GREEN + " autorisés");
							else mess.add(ChatColor.DARK_GREEN + "" + Main.getBlocs(p2.getName()) 
									+ ChatColor.GREEN + " bloc(s) protégé(s)");
							
							for(ProtectedRegion r : own.keySet()) {
								
								int x = (r.getMinimumPoint().getBlockX() + r.getMaximumPoint().getBlockX()) / 2;
								int z = (r.getMinimumPoint().getBlockZ() + r.getMaximumPoint().getBlockZ()) / 2;;
															
								mess.add(ChatColor.AQUA + "- " + ChatColor.BLUE + r.getId().substring(43) + ChatColor.AQUA 
										+ " dans " + ChatColor.DARK_AQUA + own.get(r) + ChatColor.AQUA + " aux coordonnées : " + ChatColor.DARK_AQUA + x
										+ ChatColor.AQUA + ", " + ChatColor.DARK_AQUA + z);
							}
							for(ProtectedRegion r : members.keySet()) {
								
								int x = (r.getMinimumPoint().getBlockX() + r.getMaximumPoint().getBlockX()) / 2;
								int z = (r.getMinimumPoint().getBlockZ() + r.getMaximumPoint().getBlockZ()) / 2;;
								
								mess.add(ChatColor.AQUA + "- " + ChatColor.GRAY + r.getId().substring(43) + ChatColor.AQUA + " de " 
										+ ChatColor.DARK_AQUA + Bukkit.getOfflinePlayer(UUID.fromString(r.getId().substring(6, 42))).getName() + ChatColor.AQUA
										+ " dans " + ChatColor.DARK_AQUA + members.get(r) + ChatColor.AQUA + " aux coordonnées : " + ChatColor.DARK_AQUA + x
										+ ChatColor.AQUA + ", " + ChatColor.DARK_AQUA + z);
							}
						
							int pmax = mess.size() / 9;
							if(mess.size() % 9 != 0) pmax++;
							
							if(page > pmax) page = pmax;
							
							p.sendMessage(ChatColor.YELLOW + "######### " + ChatColor.GOLD + "Claim List (Page " + page + "/" + pmax 
									+ ")" + ChatColor.YELLOW + " ##########");
							
							page--;
							
							for(int x = 0; x < 9; x++) {
								
								if((x + (page * 9)) < mess.size()) p.sendMessage(mess.get(x + (page * 9)));
							}
						}
						else p.sendMessage(ChatColor.RED + "Erreur dans la commande. " + ChatColor.DARK_RED  + "/aclaim list [player] [nb]");
					}
					else if(args[0].equalsIgnoreCase("setchest") && p.hasPermission("claim.setchest")) {
						
						chest.add(p);
						p.sendMessage(ChatColor.GREEN + "Fais un clique droit sur un coffre (simple ou double) pour définir où le blé vas arriver.");
					}
					else if(args[0].equalsIgnoreCase("info")) {
						
						if(args.length >= 3) {							

							OfflinePlayer p2 = net.IceRhal.SandboxCore.Main.h(args[1]);
							
							if(p2 == null || !p2.hasPlayedBefore()) {
								
								p.sendMessage(ChatColor.RED + "Joueur " + ChatColor.DARK_RED + args[1] + ChatColor.RED + " introuvable.");
								return true;
							}
													
							ProtectedRegion r = null;
							String world = "world";
							
							for(RegionManager rM2 : WGBukkit.getPlugin().getRegionContainer().getLoaded()) {
								
								for(ProtectedRegion reg : rM2.getRegions().values()) {
									
									if(reg.isOwner(WGBukkit.getPlugin().wrapOfflinePlayer(p2)) 
											&& reg.getId().equalsIgnoreCase("claim_" + p2.getUniqueId() + "_" + args[2])) {
										
										r = reg;
										world = rM2.getName();
										break;
									}
								}
							}
							
							if(r == null) {
								
								p.sendMessage(ChatColor.RED + "Claim introuvable.");
								return true;
							}
							
							ArrayList<String> mess = new ArrayList<String>();
							
							mess.add(ChatColor.GREEN + "Nom du Claim : " + ChatColor.DARK_GREEN 
									+ args[2].substring(0, 1).toUpperCase() + args[2].substring(1).toLowerCase());
							
							mess.add(ChatColor.GREEN + "Propriétaire : " + ChatColor.DARK_GREEN + p2.getName());
							
							mess.add(ChatColor.GREEN + "Surface : " + ChatColor.DARK_GREEN + ((r.getMaximumPoint().getBlockX() 
									- r.getMinimumPoint().getBlockX() + 1) * (r.getMaximumPoint().getBlockZ() 
											- r.getMinimumPoint().getBlockZ() + 1)) + ChatColor.GREEN + " cube(s).");
							
							int x = (r.getMaximumPoint().getBlockX() + r.getMinimumPoint().getBlockX()) / 2;
							int z = (r.getMaximumPoint().getBlockZ() + r.getMinimumPoint().getBlockZ()) / 2;
							
							mess.add(ChatColor.GREEN + "Position : monde " + ChatColor.DARK_GREEN + world + ChatColor.GREEN 
									+ ", x : " + ChatColor.DARK_GREEN + x + ChatColor.GREEN + ", z : " 
									+ ChatColor.DARK_GREEN + z); 
							
							
							if(r.getMembers().size() <= 1) {
								
								mess.add(ChatColor.GREEN + "Membre (" + ChatColor.DARK_GREEN + r.getMembers().size() + ChatColor.GREEN + ")");
							}
							else mess.add(ChatColor.GREEN + "Membres (" + ChatColor.DARK_GREEN + r.getMembers().size() + ChatColor.GREEN + ")");
							
							for(UUID id : r.getMembers().getUniqueIds()) {
								
								mess.add(ChatColor.AQUA + " - " + Bukkit.getOfflinePlayer(id).getName());
							}								
															
							int page = 1;
							
							if(args.length >= 4) {
								
								try {
									
									page = Integer.parseInt(args[3]);
									
									if(page <= 0) page = 1;
								} catch(NumberFormatException e) {
									
									page = 1;
								}
							}	
							
							int pmax = mess.size() / 9;
							if(mess.size() % 9 != 0) pmax++;
							
							if(page > pmax) page = pmax;
							
							p.sendMessage(ChatColor.YELLOW + "######### " + ChatColor.GOLD + "Claim Info (Page " + page + "/" + pmax 
									+ ")" + ChatColor.YELLOW + " ##########");
							
							page--;
							
							for(x = 0; x < 9; x++) {
								
								if((x + (page * 9)) < mess.size()) p.sendMessage(mess.get(x + (page * 9)));
							}
						}
						else p.sendMessage(ChatColor.RED + "Erreur dans la commande. " + ChatColor.DARK_RED + "/aclaim info [player] [claim] [nb]");							
					}
					else if(args[0].equalsIgnoreCase("addcredit") && p.hasPermission("claim.addcredit")) {
						
						if(args.length >= 3) {
							
							OfflinePlayer p2 = Bukkit.getOfflinePlayer(args[1]);
							
							if(p2 != null) {
								
								int nb;
								
								try {
									
									nb = Integer.parseInt(args[2]);
								} catch(NumberFormatException e) {
									
									p.sendMessage(ChatColor.RED + "Erreur dans la commande. " + ChatColor.DARK_RED + "/aclaim addcredit [player] [nb]");
									return true;
								}
								
								if(nb == 0) {
									
									p.sendMessage(ChatColor.RED + "Cela sert à rien.");
									return true;
								}
								
								double nb2 = nb;
								
								if(players.contains(p2.getUniqueId() + "")) {
									
									nb2 += players.getDouble(p2.getUniqueId() + "");
								}
								
								players.set(p2.getUniqueId() + "", nb2);
								Main.cM.f("players.yml");
								
								if(nb < 0) {
									
									p.sendMessage(ChatColor.DARK_GREEN + "" + nb + ChatColor.GREEN + " crédits ont été retiré à " + ChatColor.DARK_GREEN + p2.getName());
								}
								else if(nb > 0) {
									
									p.sendMessage(ChatColor.DARK_GREEN + "" + nb + ChatColor.GREEN + " crédits ont été ajouté à " + ChatColor.DARK_GREEN + p2.getName());
								}
								
								int h = (int) (Main.getExpireTime(p2.getUniqueId()) / 60);
								int j = h / 24;
								
								if(h < 24) p.sendMessage(ChatColor.GREEN + "Ses claims expirent désormais dans " + ChatColor.DARK_GREEN + h + ChatColor.GREEN + " heure(s).");
								else p.sendMessage(ChatColor.GREEN + "Ses claims expirent désormais dans " + ChatColor.DARK_GREEN + j + ChatColor.GREEN + " jour(s).");
							}
							else p.sendMessage(ChatColor.RED + "Joueur " + ChatColor.DARK_RED + args[1] + ChatColor.RED + " introuvable.");
						}
						else p.sendMessage(ChatColor.RED + "Erreur dans la commande. " + ChatColor.DARK_RED + "/aclaim addcredit [player] [nb]");
					}
					else if(args[0].equalsIgnoreCase("expire")) {
						
						List<String> expireList = null;
						
						if(players.contains("expireList")) expireList = players.getStringList("expireList");
						
						if(expireList == null || expireList.isEmpty()) {
							
							p.sendMessage(ChatColor.AQUA + "Aucun claim d'expiré.");
							return true;
						}
						
						if(args.length >= 2) {
							
							int id;
							boolean all = false;
							
							try {
								
								id = Integer.parseInt(args[2]);
								
							} catch(NumberFormatException | ArrayIndexOutOfBoundsException e) {
								
								id = -1;
								if(args.length >= 3 && args[2].equalsIgnoreCase("*")) all = true;
							}
							
							if(args[1].equalsIgnoreCase("remove")) {
								
								if(all && p.hasPermission("claim.expire.remove.*")) {
									
									players.set("expireList", null);
									Main.cM.f("players.yml");
									
									p.sendMessage(ChatColor.GREEN + "La liste des claims expirés est reset.");
									return true;
								}
								
								if(id < 0 || id > expireList.size()) {
									
									p.sendMessage(ChatColor.RED + "Id invalide.");
									return true;
								}
								
								expireList.remove(id);
								players.set("expireList", expireList);
								Main.cM.f("players.yml");
								
								p.sendMessage(ChatColor.GREEN + "Le claim expiré " + ChatColor.DARK_GREEN + id + ChatColor.GREEN + " est bien retiré de la liste.");
								
								return true;
							}
							
							if(args[1].equalsIgnoreCase("tp") && p.hasPermission("claim.expire.tp")) {
								
								if(id < 0 || id > expireList.size()) {
									
									p.sendMessage(ChatColor.RED + "Id invalide.");
									return true;
								}
								
								String[] r = expireList.get(id).split("[*]");
								
								Location l = new Location(Bukkit.getWorld(r[4]), Integer.parseInt(r[2]), 255, Integer.parseInt(r[3]));
								
								Material m = l.getBlock().getType();
								
								while(m == Material.AIR) {							

									if(l.getY() <= 30) {
										
										l.setX(l.getX() + 1);
										l.setY(255);
										continue;
									}
									
									l.setY(l.getY() - 1);
									m = l.getBlock().getType();
								}
								
								l.setY(l.getY() + 3);
								
								p.teleport(l, TeleportCause.COMMAND);
								l.getWorld().playSound(l, Sound.ENDERMAN_TELEPORT, 1, 1);
								
								p.sendMessage(ChatColor.GREEN + "Tu viens de te téléporter sur l'ancien claim " 
										+ ChatColor.DARK_GREEN + r[1].substring(0, 1).toUpperCase() 
										+ r[1].substring(1) + ChatColor.GREEN + " de " + ChatColor.DARK_GREEN 
										+ Bukkit.getOfflinePlayer(UUID.fromString(r[0])).getName());

								return true;	
							}
						}
													
						int page;
						int pagemax = expireList.size() / 9;
						
						if(pagemax % 9 > 0) pagemax++;
						
						try {
							
							page = Integer.parseInt(args[1]);
							
							if(page <= 0) page = 1;
							if(page > pagemax) page = pagemax;
							
						} catch(NumberFormatException | ArrayIndexOutOfBoundsException e) {
							
							page = 1;
						}
						
						p.sendMessage(ChatColor.DARK_AQUA + "#### " + ChatColor.AQUA + "Claim expiré" + ChatColor.DARK_AQUA + " - " 
								+ ChatColor.AQUA + "Page " + page + ChatColor.DARK_AQUA + " ####");
						
						for(int x = 0; x < 10; x++) {
							
							if(x >= expireList.size() + 1) break;
							
							String[] r = expireList.get(x + (page - 1)).split("[*]");
							
							p.sendMessage(ChatColor.GREEN + "" + (x + (page -1)) + ". (" + Bukkit.getOfflinePlayer(UUID.fromString(r[0])).getName() + ") " 
									+ r[1].substring(0, 1).toUpperCase() + r[1].substring(1) + " : x = " + r[2] + ", z = " + r[3] + ", world = " + r[4]);								
						}							
					}
					else if(args[0].equalsIgnoreCase("add")) {
						
						if(args.length == 4) {
							
							OfflinePlayer p1 = net.IceRhal.SandboxCore.Main.h(args[1]);
							
							if(p1 == null) {
								
								p.sendMessage(ChatColor.RED + "Joueur " + ChatColor.DARK_RED + args[1] + ChatColor.RED + " introuvable.");
								return true;
							}
							
							OfflinePlayer p2 = net.IceRhal.SandboxCore.Main.h(args[2]);
													
							if(p2 != null) {
								
								LocalPlayer p3 = WGBukkit.getPlugin().wrapOfflinePlayer(p1);
								LocalPlayer p4 = WGBukkit.getPlugin().wrapOfflinePlayer(p2);
										
								if(p2.getUniqueId() == p1.getUniqueId()) {
									
									p.sendMessage(ChatColor.RED + "Tu ne peux pas ajouter " + ChatColor.DARK_RED + p2.getName() + ChatColor.RED + " comme membre de ses terrains.");
									return true;
								}
								
								if(args[3].equalsIgnoreCase("*")) {
									
									for(ProtectedRegion r : rM.getRegions().values()) {
										
										if(r.getOwners().contains(p3)) {

											DefaultDomain d = r.getMembers();
											
											d.addPlayer(p4);
											
											r.setMembers(d);
										}
									}
									
									p.sendMessage(ChatColor.GREEN + "Le joueur " + ChatColor.DARK_GREEN + p2.getName() + ChatColor.GREEN
											+ " a été ajouté à tout les terrains de " + ChatColor.DARK_GREEN + p1.getName());
								}
								else {
									
									ProtectedRegion r = rM.getRegion("claim_" + p1.getUniqueId() + "_" + args[4].toLowerCase());
									
									if(r != null) {
										
										DefaultDomain d = r.getMembers();
										
										if(!d.contains(p3)) {
											
											d.addPlayer(p3);
											
											r.setMembers(d);
											
											p.sendMessage(ChatColor.GREEN + "Le joueur " + ChatColor.DARK_GREEN + p2.getName() + ChatColor.GREEN
													+ " a été ajoute au terrain " + ChatColor.DARK_GREEN + args[4] + ChatColor.GREEN + " de " + ChatColor.DARK_GREEN + p1.getName());
										}
										else p.sendMessage(ChatColor.RED + "Le joueur " + ChatColor.DARK_RED + p2.getName() + ChatColor.RED 
												+ " est déjà présent sur ce terrain.");				
									}
									else p.sendMessage(ChatColor.RED + "Terrain inconnu.");
								}
							}
							else p.sendMessage(ChatColor.RED + "Joueur " + ChatColor.DARK_RED + args[2] + ChatColor.RED + " introuvable.");
						}
						else p.sendMessage(ChatColor.RED + "Erreur dans la commande. " + ChatColor.DARK_RED + "/aclaim add [propriétaire] [joueur] [terrain]");
					}
					else if(args[0].equalsIgnoreCase("remove")) {
						
						p.sendMessage(ChatColor.RED + "Erreur.");
					}
					else p.sendMessage(ChatColor.RED + "Erreur dans la commande. Aide : " + ChatColor.DARK_RED + "/aclaim help");
				}
				else p.sendMessage(noperm("claim.admin"));
				
				return true;
			}
			else p.sendMessage(ChatColor.RED + "Commande " + ChatColor.GREEN + label + ChatColor.RED + " inconnue.");
		}
		else {
			
			if(args.length >= 1 && args[0].equalsIgnoreCase("update")) {
				
				for(World w : Bukkit.getWorlds()) {
					
					for(ProtectedRegion r : WGBukkit.getRegionManager(w).getRegions().values()) {

						String id = r.getId();
						
						if(id.toLowerCase().startsWith("claim_") && id.length() >= 43) {
							
							OfflinePlayer p = Bukkit.getOfflinePlayer(UUID.fromString(id.substring(6, 42)));
							
							Map<Flag<?>, Object> fl = r.getFlags();							
													
							fl.clear();
							
							r.setFlags(fl);
							
							try {
																								
								r.setFlag(DefaultFlag.GREET_MESSAGE, DefaultFlag.GREET_MESSAGE.parseInput(WGBukkit.getPlugin(), 
										sender, ChatColor.AQUA + "Tu entres sur le terrain : " + ChatColor.DARK_AQUA + id.toUpperCase().substring(43, 44) 
										+ id.substring(44).toLowerCase() + ChatColor.AQUA + " de " + ChatColor.DARK_AQUA + p.getName()));
							
								r.setFlag(DefaultFlag.FAREWELL_MESSAGE, DefaultFlag.FAREWELL_MESSAGE.parseInput(WGBukkit.getPlugin(), 
										sender, ChatColor.LIGHT_PURPLE + "Tu sors du terrain : " + ChatColor.DARK_PURPLE + id.toUpperCase().substring(43, 44) 
										+ id.substring(44).toLowerCase() + ChatColor.LIGHT_PURPLE + " de " + ChatColor.DARK_PURPLE + p.getName()));
								
								r.setFlag(DefaultFlag.ENDERDRAGON_BLOCK_DAMAGE, State.DENY);
								r.setFlag(DefaultFlag.FIRE_SPREAD, State.DENY);
								r.setFlag(j.as, State.DENY);
								r.setFlag(j.at, State.DENY);
								r.setFlag(Main.claimable, State.DENY);
							} catch (InvalidFlagFormat e) {
								e.printStackTrace();
							}							
						}
					}
				}
				
				System.out.print("[SandboxClaim] Succes claim update.");
			}			
			else sender.sendMessage("Cette commande doit etre faites par un joueur.");
		}
		
		return false;
	}
	
	boolean Integer(String s) {
		
		try {
			
			Integer.parseInt(s);
			return true;
		} catch(NumberFormatException e) {
			
			return false;
		}
		
	}
	
	static boolean resetView(final Player p, BlockVector min1, BlockVector max1) {
		
		final BlockVector min = min1.setY(30).toBlockVector();
		final BlockVector max = max1.setY(255).toBlockVector();
		
		if(region.containsKey(p)) {
			
			ArrayList<ChunkCoordIntPair> chunkList2 = delView(p, false);
			
			chunkList.put(p, chunkList2);		
						
			task.put(p, Bukkit.getScheduler().runTaskTimer(Main.plugin, new Runnable() {
				
				@Override
				public void run() {
					
					ArrayList<ChunkCoordIntPair> chunkList3 = new ArrayList<ChunkCoordIntPair>();
					
					if(chunkList.containsKey(p)) {						

						for(ChunkCoordIntPair c : chunkList.get(p)) {
							
							if(((CraftPlayer)p).getHandle().chunkCoordIntPairQueue.contains(c)) chunkList3.add(c);
						}
					}
					
					if(chunkList3.isEmpty() || !chunkList.containsKey(p)) {
						
						chunkList.remove(p);
						setView(p, min, max);
						
						BukkitTask bT = task.get(p);
											
						task.remove(p);
						bT.cancel();
					}
					else {
						
						chunkList.put(p, chunkList3);
					}				
				}			
			}, 0L, 1L));
		}
		else setView(p, min, max);
		
		BlockVector[] b = new BlockVector[2];
		
		b[0] = min;
		b[1] = max;
		
		region.put(p, b);
		
		return true;
	}	
	
	@SuppressWarnings("deprecation")
	static boolean setView(Player p, BlockVector min, BlockVector max) {
				
		ProtectedRegion reg = new ProtectedCuboidRegion("test", min, max);
		
		Set<ProtectedRegion> regs = WGBukkit.getRegionManager(p.getWorld()).
				getApplicableRegions(reg).getRegions();
			
			for(int nbX = min.getBlockX(); nbX <= max.getX(); nbX++) {
				
				for(int nbZ = min.getBlockZ(); nbZ <= max.getBlockZ(); nbZ++) {

					Location loc = p.getLocation();
					
					loc.setX(nbX);
					loc.setZ(nbZ);
					loc.setY(loc.getBlockY() + 5);
									
					p.sendBlockChange(loc, Material.STAINED_GLASS, (byte) 5); //Green Glass
				}
			}
			
			if(!regs.isEmpty()) {
								
				for(ProtectedRegion r : regs) {

					if(r.getFlags().containsKey(Main.claimable) && r.getFlag(Main.claimable) == State.ALLOW) continue;
					
					for(int nbX = min.getBlockX(); nbX <= max.getX(); nbX++) {
						
						for(int nbZ = min.getBlockZ(); nbZ <= max.getBlockZ(); nbZ++) {
							
							Location loc = p.getLocation();
							
							loc.setX(nbX);
							loc.setZ(nbZ);
							loc.setY(loc.getBlockY() + 5);
							
							if(reg.contains(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ()) && r.contains(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ())) {
								
								if(r.isOwner(p.getName())) p.sendBlockChange(loc, Material.STAINED_GLASS, (byte) 11); //Blue Glass
								else if(r.isMember(p.getName())) p.sendBlockChange(loc, Material.STAINED_GLASS, (byte) 0); //White Glass
								else p.sendBlockChange(loc, Material.STAINED_GLASS, (byte) 14); //Red Glass
							}
						}
					}
				}
			}
			
		return true;
	}
	
	@SuppressWarnings("unchecked")
	static ArrayList<ChunkCoordIntPair> delView(final Player p, boolean b) {
		
		ArrayList<Chunk> cList = new ArrayList<Chunk>();
		
		final BlockVector min2 = region.get(p)[0];
		final BlockVector max2 = region.get(p)[1];
		
		for(int X = min2.getBlockX(); X <= max2.getX(); X++) {
			
			for(int Z = min2.getBlockZ(); Z <= max2.getBlockZ(); Z++) {
				
				Chunk c = p.getLocation().getWorld().getBlockAt(X, 1, Z).getChunk();
				
				if(!cList.contains(c) && c != null) cList.add(c);
			}
		}
		
		ArrayList<ChunkCoordIntPair> chunkList2 = new ArrayList<ChunkCoordIntPair>();
		
		for(Chunk c : cList) chunkList2.add(new ChunkCoordIntPair(c.getX(), c.getZ()));
		
		for(ChunkCoordIntPair c : chunkList2) ((CraftPlayer)p).getHandle().chunkCoordIntPairQueue.add(c);
		
		if(true) region.remove(p);
		
		return chunkList2;
	}
	
	static boolean isBig(Player p, BlockVector min, BlockVector max) {
		
		int limit = Main.getLimit(p.getName(), p.getWorld());
		
		if(limit == -1) limit = 1000000;
		
		if((max.getBlockX() - min.getBlockX() + 1) * (max.getBlockZ() - min.getBlockZ() + 1) > limit) return true;
		else return false;
	}

	String noperm(String s) {
		
		return ChatColor.RED + "Tu n'as pas la permission (" + ChatColor.GREEN + s + ChatColor.RED + ") pour cette commande.";
	}

}
